<?php

declare(strict_types=1);

namespace App\Support\Constants;

use Illuminate\Support\Collection;
use ReflectionClass;

abstract class AbstractConstant
{
    public static function get(): Collection
    {
        return collect((new ReflectionClass(static::class))->getConstants());
    }

    public static function getSelectOption(): Collection
    {
        $data = static::get();
        $translatedData = [];
        foreach ($data as $key => $state) {
            $translatedData[] = [
                'name' => $key,
                'value' => $state,
                'label' => $state,
            ];
        }

        return collect($translatedData);
    }
}
