<?php

declare(strict_types=1);

namespace App\Support\Controllers;

use Illuminate\Routing\Controller;

abstract class AbstractWebController extends Controller
{
}
