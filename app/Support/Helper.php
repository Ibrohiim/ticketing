<?php

declare(strict_types=1);

if (!function_exists('money')) {
    function money(float|int $amount, bool $useDecimal = null, string $symbol = null): string
    {
        $useDecimal ??= false;
        if (!$symbol) {
            $symbol = 'Rp';
        }

        $isNegative = false;
        if ($amount < 0) {
            $isNegative = true;
            $amount = abs($amount);
        }

        $parts = [];
        $parts['symbol'] = $symbol;
        $decimal = $useDecimal ? 2 : 0;
        $parts['amount'] = number_format($amount, $decimal, ',', '.');

        $price = implode('', $parts);

        if ($isNegative) {
            return "($price)";
        }

        return $price;
    }
}

if (!function_exists('sanitize_input_number')) {
    function sanitize_input_number(string $input): int
    {
        return (int) str_replace('.', '', trim($input));
    }
}

if (!function_exists('badge_html')) {
    function badge_html(string $text, string $type, ?string $icon = null): string
    {
        $iconHtml = $icon ? sprintf('<i class="fa-solid fa-%s"></i>', $icon) : '';

        return vsprintf('<span class="badge bg-%s">%s %s</span>', [
            $type,
            $iconHtml,
            $text,
        ]);
    }
}

if (!function_exists('state_badge')) {
    function state_badge(string $label, string $state): string
    {
        $badgeType = match ($state) {
            'draft', => 'secondary',
            'in_progress', => 'info',
            'completed', 'available' => 'success',
            'cancelled', 'sold' => 'danger',
            default => 'light',
        };

        $badgeIcon = match ($state) {
            'draft' => 'triangle-exclamation',
            'in_progress' => 'bars-progress',
            'completed' => 'circle-check',
            'cancelled' => 'circle-xmark',
            default => null,
        };

        return badge_html($label, $badgeType, $badgeIcon);
    }
}
