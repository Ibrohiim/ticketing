<?php

declare(strict_types=1);

namespace App\Support\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class AbstractModel extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $perPage = 60;

    abstract protected static function newFactory(): mixed;
}
