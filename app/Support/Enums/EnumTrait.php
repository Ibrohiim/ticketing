<?php

declare(strict_types=1);

namespace App\Support\Enums;

use Illuminate\Support\Collection;

trait EnumTrait
{
    public static function list(): Collection
    {
        return collect(self::cases())->map(static fn ($case) => [
            'name' => $case->name,
            'value' => $case->value,
            'label' => $case->translated(),
        ]);
    }

    public function translated(): string
    {
        return __($this->value);
    }
}
