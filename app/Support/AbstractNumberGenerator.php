<?php

declare(strict_types=1);

namespace App\Support;

abstract class AbstractNumberGenerator
{
    protected string $model;

    protected string $prefix;

    protected int $padLength = 4;

    protected int $startSequence = 1;

    protected string $separator = '/';

    public function generate(): string
    {
        do {
            $number = $this->buildNumber();
        } while ($this->getModel()::where('number', $number)->first() instanceof $this->model);

        return $number;
    }

    private function buildNumber(): string
    {
        $number = $this->getLastSequence() > 0 ? $this->getLastSequence() + 1 : $this->startSequence;

        return vsprintf("%s{$this->getSeparator()}%s", [
            $this->getPrefix(),
            str_pad((string) $number, $this->padLength, '0', STR_PAD_LEFT),
        ]);
    }

    private function getLastSequence(): int
    {
        $lastSequence = $this->getModel()::withoutGlobalScopes()
            ->select('number')
            ->where('number', 'like', $this->getPrefix().'%')
            ->orderBy('id', 'desc')
            ->first();

        if ($lastSequence) {
            $lastNumber = explode($this->getSeparator(), $lastSequence->number);

            return (int) end($lastNumber);
        }

        return 0;
    }

    private function getSeparator(): string
    {
        return $this->separator;
    }

    private function getPrefix(): string
    {
        $prefix = collect();
        $prefix->push($this->prefix);
        $prefix->push(date('ym'));

        return $prefix->implode($this->getSeparator());
    }

    private function getModel()
    {
        return (new $this->model())->withoutRelations();
    }
}
