<?php

declare(strict_types=1);

namespace App\Support\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository
{
    public const SORT_DIRECTION_ASC = 'asc';
    public const SORT_DIRECTION_DESC = 'desc';

    protected string $model;

    /** @var Builder<Model> */
    protected Builder $query;

    protected string $defaultSort = 'created_at';

    protected string $defaultSortDirection = self::SORT_DIRECTION_DESC;

    /** @var array<string>|null */
    protected ?array $searchableColumns = [];

    /** @var array<string>|null */
    protected ?array $with = [];

    protected ?int $limit = null;

    public function __construct()
    {
        $this->makeQuery();
    }

    private function makeQuery(): void
    {
        /** @var Model $model */
        $model = new $this->model();

        $query = $model->query();

        $this->query = !empty($this->with) ? $query->with($this->with) : $query;
    }

    public function search(string $keyword): static
    {
        if (!empty($this->searchableColumns)) {
            $keyword = sprintf('%%%s%%', $keyword);

            /** @var array<string> $searchableColumns */
            $searchableColumns = $this->searchableColumns;

            $this->query = $this->query->where(function ($query) use ($searchableColumns, $keyword) {
                foreach ($searchableColumns as $column) {
                    $query->orWhere($column, 'ilike', $keyword);
                }
            });
        }

        return $this;
    }

    public function limit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @param  array<string,string>|null                         $appends
     * @return LengthAwarePaginator<Model>|Collection<int,Model>
     */
    public function paginate(array $appends = null): LengthAwarePaginator|Collection
    {
        $appends = $appends ?? [];

        $this->orderBy();

        return $this->query
            ->paginate($this->limit ?? null)
            ->appends($appends);
    }

    /**
     * @return Collection<int,Model>
     */
    public function get(): Collection|array
    {
        $this->orderBy();

        return $this->query
            ->limit($this->limit ?? null)
            ->get();
    }

    protected function orderBy()
    {
        $this->query->orderBy($this->defaultSort, $this->defaultSortDirection);

        return $this;
    }
}
