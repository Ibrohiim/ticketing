<?php

declare(strict_types=1);

namespace App\Support;

use App\Support\Constants\AbstractConstant;

class Locale extends AbstractConstant
{
    public const EN = 'en';
    public const ID = 'id';
}
