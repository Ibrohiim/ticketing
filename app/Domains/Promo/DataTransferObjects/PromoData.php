<?php

declare(strict_types=1);

namespace App\Domains\Promo\DataTransferObjects;

use App\Domains\Promo\Models\Promo;
use Carbon\Carbon;
use Spatie\LaravelData\Data;

class PromoData extends Data
{
    public function __construct(
        public ?int $id,
        public ?int $ticket_id,
        public ?string $discount_name,
        public ?string $discount_code,
        public ?string $type,
        public ?string $type_formatted,
        public ?float $quota,
        public ?float $percentage,
        public ?float $nominal,
        public ?float $min_transaction,
        public ?string $period_start_date,
        public ?string $period_start_date_formatted,
        public ?string $period_finish_date,
        public ?string $period_finish_date_formatted,
        public ?string $state,
        public ?string $state_formatted,
    ) {
    }

    public static function fromModel(Promo $promo): self
    {
        return new self(
            id: $promo->id,
            ticket_id: $promo->ticket_id,
            discount_name: $promo->discount_name,
            discount_code: $promo->discount_code,
            quota: $promo->quota,
            type: $promo->type,
            type_formatted: self::discountTypeFormatted($promo),
            percentage: $promo->percentage,
            nominal: $promo->nominal,
            min_transaction: $promo->min_transaction,
            period_start_date: $promo->period_start_date,
            period_start_date_formatted: Carbon::parse($promo->period_start_date)->format(config('rnd.human_date_time_format')),
            period_finish_date: $promo->period_finish_date,
            period_finish_date_formatted: Carbon::parse($promo->period_finish_date)->format(config('rnd.human_date_time_format')),
            state: $promo->state,
            state_formatted: self::stateFormatted($promo),
        );
    }

    private static function stateFormatted(Promo $promo): string
    {
        $stateFormatted = '&ndash;';
        if ($promo->state) {
            $stateFormatted = state_badge(
                $promo->state,
                $promo->state
            );
        }

        return $stateFormatted;
    }

    private static function discountTypeFormatted(Promo $promo): string
    {
        $typeFormatted = '&ndash;';
        if ($promo->type) {
            $typeFormatted = state_badge(
                $promo->type,
                $promo->type
            );
        }

        return $typeFormatted;
    }
}
