<?php

declare(strict_types=1);

namespace App\Domains\Promo\Controllers\API;

use App\Domains\Promo\Actions\DeletePromo;
use App\Domains\Promo\Actions\SavePromo;
use App\Domains\Promo\Constants\PromoType;
use App\Domains\Promo\Constants\State;
use App\Domains\Promo\DataTransferObjects\PromoData;
use App\Domains\Promo\Models\Promo;
use App\Domains\Promo\Requests\SavePromoRequest;
use App\Domains\Event\Models\Event;
use App\Domains\Ticket\Models\Ticket;
use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractAPIController;
use App\Support\Exceptions\JsonResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class PromoController extends AbstractAPIController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::BROWSE_PROMO))->only(['index']);
        $this->middleware(sprintf('permission:%s', Permission::READ_PROMO))->only(['show']);
        $this->middleware(sprintf('permission:%s', Permission::ADD_PROMO))->only(['store']);
        $this->middleware(sprintf('permission:%s', Permission::EDIT_PROMO))->only(['update']);
        $this->middleware(sprintf('permission:%s', Permission::DELETE_PROMO))->only(['destroy']);
    }

    public function index(Promo $promo): JsonResponse
    {
        return $this->sendJsonResponse(PromoData::collection($promo->get()));
    }

    public function getByTicketId(Ticket $ticket): JsonResponse
    {
        return $this->sendJsonResponse(PromoData::collection($ticket->promo()->get()));
    }

    public function getByEventId(Event $event): JsonResponse
    {
        return $this->sendJsonResponse(PromoData::collection($event->promo()->get()));
    }

    public function store(SavePromoRequest $request): JsonResponse
    {
        SavePromo::dispatchSync(new Promo, PromoData::from($request->all()));

        return $this->sendJsonResponse([
            'message' => 'stored_data', ['data' => 'promo ticket'],
        ]);
    }

    public function update(SavePromoRequest $request, Promo $promo): JsonResponse
    {
        SavePromo::dispatchSync($promo, PromoData::from($request->all()));

        return $this->sendJsonResponse([
            'message' => 'updated_data', ['data' => 'Promo ticket'],
        ]);
    }

    public function show(Promo $promo): JsonResponse
    {
        return $this->sendJsonResponse(
            PromoData::from($promo)
        );
    }

    /**
     * @throws JsonResponseException
     */
    public function destroy(Event $event, $promo): JsonResponse
    {
        try {
            $event->promo->find($promo)->delete();

            return $this->sendJsonResponse([
                'message' => 'deleted_data', ['data' => 'promo'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }

    public function state(): JsonResponse
    {
        return $this->sendJsonResponse(State::get()->values()->toArray());
    }

    public function promoType(): JsonResponse
    {
        return $this->sendJsonResponse(PromoType::get()->values()->toArray());
    }
}
