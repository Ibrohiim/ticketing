<?php

declare(strict_types=1);

namespace App\Domains\DiscountTicket\Controllers\Web;

use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractWebController;
use Illuminate\Contracts\View\View;

class DiscountTicketController extends AbstractWebController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::MANAGE_DISCOUNT_TICKETS));
    }

    public function index(): View
    {
        return view('discount-tickets.index');
    }
}
