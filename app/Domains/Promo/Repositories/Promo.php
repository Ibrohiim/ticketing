<?php

declare(strict_types=1);

namespace App\Domains\Promo\Repositories;

use App\Support\Repositories\AbstractRepository;

class Promo extends AbstractRepository
{
    protected string $model = \App\Domains\Promo\Models\Promo::class;
    protected ?array $searchableColumns = ['Discount_name', 'discount_code'];
    protected string $defaultSort = 'Discount_name';
    protected string $defaultSortDirection = parent::SORT_DIRECTION_ASC;
    protected ?array $with = ['ticket'];

    public function ticket(int $ticketId): static
    {
        $this->query = $this->query->where('ticket_id', $ticketId);

        return $this;
    }
}
