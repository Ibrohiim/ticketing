<?php

declare(strict_types=1);

namespace App\Domains\Promo\Scopes;

use App\Domains\User\Constants\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class OwnPromoScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        /** @var \App\Domains\User\Models\User|null $authenticatable */
        $authenticatable = auth()->user();

        if ($authenticatable && $authenticatable->hasRole(Role::USER)) {
            $builder->whereHas('ticket', function (Builder $query) use ($authenticatable) {
                $query->whereHas('event', function (Builder $query) use ($authenticatable) {
                    $query->where('use_id', $authenticatable->id);
                });
            });
        }
    }
}
