<?php

declare(strict_types=1);

namespace App\Domains\Promo\Actions;

use App\Domains\Promo\Constants\PromoType;
use App\Domains\Promo\Constants\State;
use App\Domains\Promo\DataTransferObjects\PromoData;
use App\Domains\Promo\Models\Promo;
use App\Support\Actions\AbstractAction;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;

class SavePromo extends AbstractAction
{
    public function __construct(
        private readonly Promo $promo,
        private readonly PromoData $data
    ) {
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $type = ($this->data->type === 'nominal') ? PromoType::NOMINAL : PromoType::PERCENTAGE;
            $state = ($this->data->quota > 0) ? State::AVAILABLE : State::SOLD;

            $this->promo->ticket()->associate($this->data->ticket_id);

            $this->promo->fill([
                'discount_name' => $this->data->discount_name,
                'discount_code' => $this->data->discount_code,
                'quota' => $this->data->quota,
                'type' => $type,
                'percentage' => $this->data->percentage,
                'nominal' => $this->data->nominal,
                'min_transaction' => $this->data->min_transaction,
                'period_start_date' => $this->data->period_start_date,
                'period_finish_date' => $this->data->period_finish_date,
                'state' => $state
            ])->save();
        });
    }
}
