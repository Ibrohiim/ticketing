<?php

declare(strict_types=1);

namespace App\Domains\Promo\Actions;

use App\Domains\Promo\Models\Promo;
use App\Support\Actions\AbstractAction;

class DeletePromo extends AbstractAction
{
    public function __construct(
        private readonly Promo $promo
    ) {
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->promo->delete();
    }
}
