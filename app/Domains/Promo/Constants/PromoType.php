<?php

declare(strict_types=1);

namespace App\Domains\Promo\Constants;

use App\Support\Constants\AbstractConstant;

class PromoType extends AbstractConstant
{
    public const PERCENTAGE = 'percentage';
    public const NOMINAL = 'nominal';
}
