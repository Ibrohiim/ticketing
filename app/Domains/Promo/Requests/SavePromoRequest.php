<?php

declare(strict_types=1);

namespace App\Domains\Promo\Requests;

use App\Domains\User\Constants\Role;
use Illuminate\Foundation\Http\FormRequest;

class SavePromoRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->hasRole([Role::ADMINISTRATOR, Role::USER]);
    }

    public function rules(): array
    {
        return [
            'ticket_id' => ['required'],
            'discount_name' => ['required'],
            'discount_code' => ['required'],
        ];
    }
}
