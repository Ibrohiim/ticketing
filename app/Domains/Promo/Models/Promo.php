<?php

declare(strict_types=1);

namespace App\Domains\Promo\Models;

use App\Domains\Promo\Scopes\OwnPromoScope;
use App\Domains\Ticket\Models\Ticket;
use App\Support\Models\AbstractModel;
use Database\Factories\PromoFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Promo extends AbstractModel
{
    protected $table = 'promo_tickets';
    protected $fillable = [
        'discount_name',
        'discount_code',
        'quota',
        'type',
        'percentage',
        'nominal',
        'min_transaction',
        'period_start_date',
        'period_finish_date',
        'state',

    ];
    protected $casts = [
        'quota' => 'float',
        'percentage' => 'float',
        'nominal' => 'float',
        'min_transaction' => 'float',
    ];

    // protected static function booted(): void
    // {
    //     parent::booted();
    //     static::addGlobalScope(new OwnDiscountTicketScope());
    // }

    protected static function newFactory(): PromoFactory
    {
        return PromoFactory::new();
    }

    public function ticket(): BelongsTo
    {
        return $this->belongsTo(Ticket::class);
    }

    public function event()
    {
        return $this->ticket->event;
    }
}
