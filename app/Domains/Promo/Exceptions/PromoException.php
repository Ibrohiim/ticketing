<?php

declare(strict_types=1);

namespace App\Domains\Promo\Exceptions;

use App\Support\Exceptions\AbstractException;

class PromoException extends AbstractException
{
}
