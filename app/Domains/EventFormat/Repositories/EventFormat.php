<?php

declare(strict_types=1);

namespace App\Domains\EventFormat\Repositories;

use App\Support\Repositories\AbstractRepository;

class EventFormat extends AbstractRepository
{
    protected string $model = \App\Domains\EventFormat\Models\EventFormat::class;
    protected ?array $searchableColumns = ['name'];
    protected string $defaultSort = 'name';
    protected string $defaultSortDirection = parent::SORT_DIRECTION_ASC;
}
