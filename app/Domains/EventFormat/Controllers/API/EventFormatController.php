<?php

declare(strict_types=1);

namespace App\Domains\EventFormat\Controllers\API;

use App\Domains\EventFormat\Actions\SaveEventFormat;
use App\Domains\EventFormat\DataTransferObjects\EventFormatData;
use App\Domains\EventFormat\Models\EventFormat;
use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractAPIController;
use App\Support\Exceptions\JsonResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class EventFormatController extends AbstractAPIController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::BROWSE_EVENT_FORMATS))->only(['index']);
        $this->middleware(sprintf('permission:%s', Permission::ADD_EVENT_FORMAT))->only(['store']);
        $this->middleware(sprintf('permission:%s', Permission::EDIT_EVENT_FORMAT))->only(['update']);
        $this->middleware(sprintf('permission:%s', Permission::DELETE_EVENT_FORMAT))->only(['destroy']);
    }

    public function index(Request $request): JsonResponse
    {
        $repository = (new \App\Domains\EventFormat\Repositories\EventFormat());

        if ($request->filled('q')) {
            $repository->search($request->input('q'));
        }

        $eventFormat = $request->boolean('paginate') ? $repository->paginate($request->all()) : $repository->get();

        return $this->sendJsonResponse(EventFormatData::collection($eventFormat));
    }

    public function store(EventFormatData $data): JsonResponse
    {
        SaveEventFormat::dispatchSync(new EventFormat(), $data);

        return $this->sendJsonResponse([
            'message' => 'stored_data', ['data' => 'event_format'],
        ]);
    }

    public function update(EventFormat $eventFormat, EventFormatData $data): JsonResponse
    {
        SaveEventFormat::dispatchSync($eventFormat, $data);

        return $this->sendJsonResponse([
            'message' => 'updated_data', ['data' => 'event_format'],
        ]);
    }

    // /**
    //  * @throws JsonResponseException
    //  */
    public function destroy(EventFormat $eventFormat): JsonResponse
    {
        try {
            $eventFormat->delete();

            return $this->sendJsonResponse([
                'message' => 'deleted_data', ['data' => 'event_format'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }
}
