<?php

declare(strict_types=1);

namespace App\Domains\EventFormat\Controllers\Web;

use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractWebController;
use Illuminate\Contracts\View\View;

class EventFormatController extends AbstractWebController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::MANAGE_EVENT_FORMATS));
    }

    public function index(): View
    {
        return view('event-formats.index');
    }
}
