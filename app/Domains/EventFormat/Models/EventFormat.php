<?php

declare(strict_types=1);

namespace App\Domains\EventFormat\Models;

use App\Support\Models\AbstractModel;
use Database\Factories\EventFormatFactory;

class EventFormat extends AbstractModel
{
    protected $fillable = ['name', 'description'];

    protected static function newFactory(): EventFormatFactory
    {
        return EventFormatFactory::new();
    }
}
