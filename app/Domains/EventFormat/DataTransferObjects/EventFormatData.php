<?php

declare(strict_types=1);

namespace App\Domains\EventFormat\DataTransferObjects;

use Spatie\LaravelData\Data;

class EventFormatData extends Data
{
    public function __construct(
        public ?int $id,
        public string $name,
        public ?string $description
    ) {
    }
}
