<?php

declare(strict_types=1);

namespace App\Domains\EventFormat\Actions;

use App\Domains\EventFormat\DataTransferObjects\EventFormatData;
use App\Domains\EventFormat\Models\EventFormat;
use App\Support\Actions\AbstractAction;

class SaveEventFormat extends AbstractAction
{
    public function __construct(
        private readonly EventFormat $eventFormat,
        private readonly EventFormatData $data
    ) {
    }

    public function handle(): void
    {
        $this->eventFormat->fill($this->data->toArray())->save();
    }
}
