<?php

declare(strict_types=1);

namespace App\Domains\Event\DataTransferObjects;

use App\Domains\Event\Models\Event;
use App\Domains\EventCategory\DataTransferObjects\EventCategoryData;
use App\Domains\EventFormat\DataTransferObjects\EventFormatData;
use App\Domains\User\DataTransferObjects\UserData;
use Carbon\Carbon;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Lazy;

class EventData extends Data
{
    public function __construct(
        public ?int $id,
        public ?int $user_id,
        public ?int $event_format_id,
        public ?int $event_category_id,
        public string $name,
        public ?string $start_date,
        public ?string $start_date_formatted,
        public ?string $finish_date,
        public ?string $finish_date_formatted,
        public ?string $state,
        public ?string $state_formatted,
        public ?string $place,
        public ?string $address,
        public ?string $city,
        public ?string $description,
        public ?string $terms_condition,
        public ?string $event_link,
        public ?string $banner,
        public ?string $type,
        public Lazy|EventFormatData|null $event_format,
        public Lazy|EventCategoryData|null $event_category,
        public Lazy|UserData|null $user,
        // public Lazy|DataCollection|null $tickets,
    ) {
    }

    public static function fromModel(Event $event): self
    {
        return new self(
            id: $event->id,
            user_id: $event->user_id,
            event_format_id: $event->event_format_id,
            event_category_id: $event->event_category_id,
            name: $event->name,
            start_date: $event->start_date,
            start_date_formatted: Carbon::parse($event->start_date)->translatedFormat(config('event.human_date_format')),
            finish_date: $event->finish_date,
            finish_date_formatted: self::finishDateFormatted($event),
            state: $event->state,
            state_formatted: self::stateFormatted($event),
            place: $event->place,
            address: $event->address,
            city: $event->city,
            description: $event->description,
            terms_condition: $event->terms_condition,
            event_link: $event->event_link,
            banner: $event->banner,
            type: $event->type,
            event_format: Lazy::create(static fn () => EventFormatData::from($event->eventFormat)),
            event_category: Lazy::create(static fn () => EventCategoryData::from($event->eventCategory)),
            user: Lazy::create(static fn () => UserData::from($event->user)),
            // tickets: Lazy::create(static fn () => TicketData::collection($event->tickets)),
        );
    }

    private static function stateFormatted(Event $event): string
    {
        $stateFormatted = '&ndash;';
        if ($event->state) {
            $stateFormatted = state_badge(
                ($event->state),
                $event->state
            );
        }

        return $stateFormatted;
    }

    private static function finishDateFormatted(Event $event): string
    {
        if ($event->finish_date) {
            return Carbon::parse($event->finish_date)->translatedFormat(config('event.human_date_format'));
        }

        return '&ndash;';
    }
}
