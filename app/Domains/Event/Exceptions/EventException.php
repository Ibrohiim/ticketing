<?php

declare(strict_types=1);

namespace App\Domains\Event\Exceptions;

use App\Domains\Event\Models\Event;
use App\Support\Exceptions\AbstractException;

class EventException extends AbstractException
{
    public static function invalidFile(): self
    {
        return new self("Invalid File");
    }
}
