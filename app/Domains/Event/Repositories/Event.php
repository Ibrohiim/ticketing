<?php

declare(strict_types=1);

namespace App\Domains\Event\Repositories;

use App\Domains\Event\Enums\SortData;
use App\Support\Repositories\AbstractRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class Event extends AbstractRepository
{
    protected string $model = \App\Domains\Event\Models\Event::class;
    protected ?array $searchableColumns = ['name'];
    protected string $defaultSort = 'start_date';
    protected string $defaultSortDirection = parent::SORT_DIRECTION_DESC;
    protected ?array $with = ['user', 'eventCategory', 'eventFormat'];

    public function paginate(?array $appends = []): LengthAwarePaginator|EloquentCollection
    {
        return $this->query
            ->orderBy($this->defaultSort, $this->defaultSortDirection)
            ->orderBy('created_at', 'desc')
            ->paginate()
            ->appends($appends);
    }

    public function get(): EloquentCollection|array
    {
        return $this->query
            ->orderBy($this->defaultSort, $this->defaultSortDirection)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function search($keyword): static
    {
        if ($this->searchableColumns) {
            $keyword = sprintf('%%%s%%', $keyword);

            $this->query = $this->query->where(function ($query) use ($keyword) {
                foreach ($this->searchableColumns as $index => $column) {
                    0 === $index
                        ? $query->where($column, 'ilike', $keyword)
                        : $query->orWhere($column, 'ilike', $keyword);
                }

                $query->orWhereHas('user', function ($query) use ($keyword) {
                    foreach ($this->searchableColumns as $index => $column) {
                        0 === $index
                            ? $query->where($column, 'ilike', $keyword)
                            : $query->orWhere($column, 'ilike', $keyword);
                    }
                });
            });
        }

        return $this;
    }

    public function dates(Carbon $begin, Carbon $end): static
    {
        $this->query = $this->query->whereBetween('start_date', [$begin->format('Y-m-d'), $end->format('Y-m-d')]);

        return $this;
    }

    public function eventCategory(int $eventCategoryId): static
    {
        $this->query = $this->query->where('event_category_id', $eventCategoryId);

        return $this;
    }

    public function eventFormat(int $eventFormatId): static
    {
        $this->query = $this->query->where('event_format_id', $eventFormatId);

        return $this;
    }

    public function user(int $userId): static
    {
        $this->query = $this->query->where('user_id', $userId);

        return $this;
    }

    public function sortBy(string $column): static
    {
        switch ($column) {
            case SortData::UpdatedDateLatest->value:
                $this->defaultSort = 'updated_at';
                $this->defaultSortDirection = static::SORT_DIRECTION_DESC;
                break;
            case SortData::UpdatedDateOldest->value:
                $this->defaultSort = 'updated_at';
                $this->defaultSortDirection = static::SORT_DIRECTION_ASC;
                break;

            case SortData::FinishDateLatest->value:
                $this->defaultSort = 'finish_date';
                $this->defaultSortDirection = static::SORT_DIRECTION_DESC;
                break;
            case SortData::FinishDateOldest->value:
                $this->defaultSort = 'finish_date';
                $this->defaultSortDirection = static::SORT_DIRECTION_ASC;
                break;

            case SortData::FinishDateLatest->value:
                $this->defaultSort = 'finish_date';
                $this->defaultSortDirection = static::SORT_DIRECTION_DESC;
                break;
            case SortData::FinishDateOldest->value:
                $this->defaultSort = 'finish_date';
                $this->defaultSortDirection = static::SORT_DIRECTION_ASC;
                break;
        }

        return $this;
    }

    public function state(string $state): static
    {
        $this->query = $this->query->where('state', $state);

        return $this;
    }

    // public function ticket(int $ticketId): static
    // {
    //     $this->query = $this->query->whereHas('tickets', function ($query) use ($ticketId) {
    //         $query->whereHas('ticketId', function ($query) use ($ticketId) {
    //             $query->whereHas('ticketId', function ($query) use ($ticketId) {
    //                 $query->where('ticketId_id', $ticketId);
    //             });
    //         });
    //     });

    //     return $this;
    // }
}
