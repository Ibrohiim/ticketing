<?php

declare(strict_types=1);

namespace App\Domains\Event\Enums;

use App\Support\Enums\EnumTrait;

enum SortData: string
{
    use EnumTrait;

    case StartDateLatest = 'start_date_latest';
    case StartDateOldest = 'start_date_oldest';

    case FinishDateLatest = 'finish_date_latest';
    case FinishDateOldest = 'finish_date_oldest';

    case UpdatedDateLatest = 'updated_date_latest';
    case UpdatedDateOldest = 'updated_date_oldest';
}
