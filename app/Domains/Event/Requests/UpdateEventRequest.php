<?php

declare(strict_types=1);

namespace App\Domains\Event\Requests;

use App\Domains\Event\Constants\State;
use App\Domains\Event\DataTransferObjects\EventData;
use App\Domains\Event\Models\Event;
use App\Domains\User\Constants\Permission;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEventRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->hasPermissionTo(Permission::EDIT_EVENT);
    }

    public function rules(): array
    {
        $event = EventData::from($this->route()?->parameter('event'));

        $rules = [
            'name' => ['required'],
        ];

        if ($this->input('state') === State::IN_PROGRESS) {
            $rules += [
                'event_category_id' => ['required'],
                'event_format_id' => ['required'],
                'start_date' => ['required'],
                'finish_date' => ['required'],
                'start_time' => ['required'],
                'finish_time' => ['required'],
                'place' => ['required'],
                'address' => ['required'],
                'city' => ['required'],
                'event_link' => ['required'],
                'description' => ['required'],
                'terms_condition' => ['required'],
            ];
        }

        return $rules;
    }
}
