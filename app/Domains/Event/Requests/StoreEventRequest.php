<?php

declare(strict_types=1);

namespace App\Domains\Event\Requests;

use App\Domains\User\Constants\Permission;
use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->hasPermissionTo(Permission::ADD_EVENT);
    }

    public function rules(): array
    {
        return [
            'name' => ['required'],
        ];
    }
}
