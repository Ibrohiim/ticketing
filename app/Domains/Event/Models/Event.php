<?php

declare(strict_types=1);

namespace App\Domains\Event\Models;

use App\Domains\Promo\Models\Promo;
use App\Domains\EventCategory\Models\EventCategory;
use App\Domains\EventFormat\Models\EventFormat;
use App\Domains\Ticket\Models\Ticket;
use App\Domains\User\Models\HasUserTrait;
use App\Domains\User\Models\User;
use App\Domains\User\Scopes\OwnScope;
use App\Support\Models\AbstractModel;
use Database\Factories\EventFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\Storage;

class Event extends AbstractModel
{
    use HasUserTrait;

    protected $fillable = [
        'name',
        'start_date',
        'finish_date',
        'start_time',
        'finish_time',
        'place',
        'address',
        'city',
        'description',
        'terms_condition',
        'event_link',
        'type',
        'state',
        'banner'
    ];

    protected $casts = [];

    protected static function booted(): void
    {
        parent::booted();
        static::addGlobalScope(new OwnScope());
    }

    protected static function newFactory(): EventFactory
    {
        return EventFactory::new();
    }

    public function eventFormat(): BelongsTo
    {
        return $this->belongsTo(EventFormat::class);
    }

    public function eventCategory(): BelongsTo
    {
        return $this->belongsTo(EventCategory::class);
    }

    public function ticket(): HasMany
    {
        return $this->hasMany(Ticket::class);
    }

    public function promo(): HasManyThrough
    {
        return $this->hasManyThrough(Promo::class, Ticket::class);
    }

    public function banner(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset(Storage::url($value ? $value : 'banner/no-image-banner.png')),
        );
    }
}
