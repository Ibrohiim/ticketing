<?php

declare(strict_types=1);

namespace App\Domains\Event\Constants;

use App\Support\Constants\AbstractConstant;

class State extends AbstractConstant
{
    public const DRAFT = 'draft';
    public const IN_PROGRESS = 'in_progress';
    public const COMPLETED = 'completed';
    public const CANCELLED = 'cancelled';
}
