<?php

declare(strict_types=1);

namespace App\Domains\Event\Controllers\Web;

use App\Domains\Event\Models\Event;
use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractWebController;
use Illuminate\Contracts\View\View;

class EventController extends AbstractWebController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::MANAGE_EVENTS));
    }

    public function index(): View
    {
        return view('events.index');
    }

    public function show(Event $event): View
    {
        return view('events.show', compact('event'));
    }
}
