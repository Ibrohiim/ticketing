<?php

declare(strict_types=1);

namespace App\Domains\Event\Controllers\API;

use App\Domains\Event\Constants\State;
use App\Support\Controllers\AbstractAPIController;
use Illuminate\Http\JsonResponse;

class EventStateController extends AbstractAPIController
{
    public function types(): JsonResponse
    {
        return $this->sendJsonResponse(
            State::getSelectOption()->sortBy('label')->values()
        );
    }
}
