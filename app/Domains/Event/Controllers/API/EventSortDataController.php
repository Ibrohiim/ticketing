<?php

declare(strict_types=1);

namespace App\Domains\Event\Controllers\API;

use App\Domains\Event\Enums\SortData;
use App\Support\Controllers\AbstractAPIController;
use Illuminate\Http\JsonResponse;

class EventSortDataController extends AbstractAPIController
{
    public function types(): JsonResponse
    {
        return $this->sendJsonResponse(
            SortData::list()->sortBy('label')->values()
        );
    }
}
