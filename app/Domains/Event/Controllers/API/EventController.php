<?php

declare(strict_types=1);

namespace App\Domains\Event\Controllers\API;

use App\Domains\Event\Actions\StoreEvent;
use App\Domains\Event\Actions\UpdateBanner;
use App\Domains\Event\Actions\UpdateEvent;
use App\Domains\Event\Constants\State;
use App\Domains\Event\DataTransferObjects\EventData;
use App\Domains\Event\Models\Event;
use App\Domains\Event\Requests\StoreEventRequest;
use App\Domains\Event\Requests\UpdateBannerRequest;
use App\Domains\Event\Requests\UpdateEventRequest;
use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractAPIController;
use App\Support\Exceptions\JsonResponseException;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class EventController extends AbstractAPIController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::BROWSE_EVENTS))->only(['index']);
        $this->middleware(sprintf('permission:%s', Permission::READ_EVENT))->only(['show']);
        $this->middleware(sprintf('permission:%s', Permission::ADD_EVENT))->only(['store']);
        $this->middleware(sprintf('permission:%s', Permission::EDIT_EVENT))->only(['update']);
        $this->middleware(sprintf('permission:%s', Permission::DELETE_EVENT))->only(['destroy']);
    }

    public function index(Request $request): JsonResponse
    {
        $repository = new \App\Domains\Event\Repositories\Event();

        $request
            ->whenFilled('q', function () use ($repository, $request) {
                $repository->search($request->input('q'));
            })
            ->whenFilled('user', function () use ($repository, $request) {
                $repository->user($request->integer('user'));
            })
            ->whenFilled('event_format', function () use ($repository, $request) {
                $repository->eventFormat($request->integer('event_format'));
            })
            ->whenFilled('event_category', function () use ($repository, $request) {
                $repository->eventCategory($request->integer('event_category'));
            })
            ->whenFilled('dates', function () use ($request, $repository) {
                [$begin, $end] = explode(',', $request->input('dates'));
                $repository->dates(Carbon::parse($begin), Carbon::parse($end));
            })
            ->whenFilled('sort_data', function () use ($request, $repository) {
                $repository->sortBy($request->input('sort_data'));
            })
            ->whenFilled('state', function () use ($request, $repository) {
                $repository->state($request->input('state'));
            })
            ->whenFilled('limit', function () use ($request, $repository) {
                $repository->limit($request->integer('limit'));
            });

        $events = $request->boolean('paginate') ? $repository->paginate($request->all()) : $repository->get();

        return $this->sendJsonResponse(
            EventData::collection($events)
                ->include('user', 'event_category', 'event_format',)
        );
    }

    public function store(StoreEventRequest $request): JsonResponse
    {
        StoreEvent::dispatchSync(EventData::from($request));

        return $this->sendJsonResponse([
            'message' => 'stored_data', ['data' => 'event'],
        ]);
    }

    public function update(UpdateEventRequest $request, Event $event): JsonResponse
    {
        UpdateEvent::dispatchSync($event, EventData::from($request));

        return $this->sendJsonResponse([
            'message' => 'updated_data', ['data' => 'event'],
        ]);
    }

    public function show(Event $event): JsonResponse
    {
        return $this->sendJsonResponse(
            EventData::from($event)->include(
                'event_format',
                'event_category',
            )
        );
    }

    /**
     * @throws JsonResponseException
     */
    public function destroy(Event $event): JsonResponse
    {
        try {
            $event->delete();

            return $this->sendJsonResponse([
                'message' => 'deleted_data', ['data' => 'event deleted'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }

    public function updateBanner(UpdateBannerRequest $request, Event $event): JsonResponse
    {
        try {
            UpdateBanner::dispatchSync(
                event: $event,
                banner: $request->file('banner')
            );

            return $this->sendJsonResponse([
                'message' => 'Profile picture uploaded successfully!', ['data' => 'banner'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }

    public function state(): JsonResponse
    {
        return $this->sendJsonResponse(State::get()->values()->toArray());
    }
}
