<?php

declare(strict_types=1);

namespace App\Domains\Event\Actions;

use App\Domains\Event\DataTransferObjects\EventData;
use App\Domains\Event\Models\Event;
use App\Support\Actions\AbstractAction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateEvent extends AbstractAction
{
    public function __construct(
        private readonly Event $event,
        private readonly EventData $data
    ) {
        // $this->setup();
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $this->event->eventCategory()->associate($this->data->event_category_id);
            $this->event->eventFormat()->associate($this->data->event_format_id);

            $this->event->fill([
                'name' => $this->data->name,
                'start_date' => $this->data->start_date,
                'finish_date' => $this->data->finish_date,
                'place' => $this->data->place,
                'address' => $this->data->address,
                'city' => $this->data->city,
                'event_link' => $this->data->event_link,
                'description' => $this->data->description,
                'terms_condition' => $this->data->terms_condition,
                'state' => $this->data->state,
            ])->save();
        });
    }
}
