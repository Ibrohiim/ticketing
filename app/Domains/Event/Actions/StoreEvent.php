<?php

declare(strict_types=1);

namespace App\Domains\Event\Actions;

use App\Domains\Event\DataTransferObjects\EventData;
use App\Domains\Event\Models\Event;
use App\Domains\SampleOrder\Constants\RequestType;
use App\Support\Actions\AbstractAction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StoreEvent extends AbstractAction
{
    private Event $event;

    public function __construct(private readonly EventData $data)
    {
        $this->setup();
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $this->associateRelations();

            $this->event->fill([
                'name' => $this->data->name,
                'start_date' => $this->data->start_date,
                'finish_date' => $this->data->finish_date,
                'type' => $this->data->type,
                'place' => $this->data->place,
                'address' => $this->data->address,
                'city' => $this->data->city,
                'description' => $this->data->description,
                'terms_condition' => $this->data->terms_condition,
            ]);

            $this->event->save();
        });
    }

    private function associateRelations(): void
    {
        $userId = auth()->user()->id;
        $eventCategoryId = $this->data->event_category_id ?? $this->data->event_category_id;
        $eventFormatId = $this->data->event_format_id ?? $this->data->event_format_id;

        $this->event->eventCategory()->associate($eventCategoryId);
        $this->event->eventFormat()->associate($eventFormatId);
        $this->event->user()->associate($userId);
    }

    private function setup(): void
    {
        $this->event = new Event();
    }
}
