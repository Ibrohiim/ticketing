<?php

declare(strict_types=1);

namespace App\Domains\Event\Actions;

use App\Domains\Event\Exceptions\EventException;
use App\Domains\Event\Models\Event;
use App\Domains\User\Exceptions\UserException;
use App\Domains\User\Models\User;
use App\Domains\User\Models\UserBasicInformation;
use App\Support\Actions\AbstractAction;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UpdateBanner extends AbstractAction
{
    public function __construct(
        private readonly Event $event,
        protected $banner,
    ) {
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            if (!$this->banner instanceof UploadedFile) {
                throw EventException::invalidFile();
            }

            if ($this->event->banner !== asset(Storage::url('banner/no-image-banner.png'))) {
                $previousPath = $this->event->getRawOriginal('banner');
                Storage::delete($previousPath);
            }

            $link = Storage::put('/public/banner', $this->banner);

            $this->event->fill([
                'banner' => $link,
            ])->save();
        });
    }
}
