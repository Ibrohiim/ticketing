<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Constants;

use App\Support\Constants\AbstractConstant;

class CategoryTicket extends AbstractConstant
{
    public const FREE = 'free';
    public const PAID = 'paid';
}
