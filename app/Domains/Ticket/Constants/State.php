<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Constants;

use App\Support\Constants\AbstractConstant;

class State extends AbstractConstant
{
    public const SOLD = 'sold';
    public const AVAILABLE = 'available';
}
