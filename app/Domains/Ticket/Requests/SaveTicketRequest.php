<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Requests;

use App\Domains\User\Constants\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaveTicketRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->hasRole([Role::ADMINISTRATOR, Role::USER]);
    }

    public function rules(): array
    {
        return [
            'ticket_name' => ['required'],
        ];
    }
}
