<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Scopes;

use App\Domains\User\Constants\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class OwnTicketScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        /** @var \App\Domains\User\Models\User|null $authenticatable */
        $authenticatable = auth()->user();

        if ($authenticatable && $authenticatable->hasRole(Role::USER)) {
            $builder->whereHas('event', function (Builder $query) use ($authenticatable) {
                $query->whereHas('user', function (Builder $query) use ($authenticatable) {
                    $query->where('id', $authenticatable->id);
                });
            });
        }
    }
}
