<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Controllers\API;

use App\Domains\Event\Models\Event;
use App\Domains\Ticket\Actions\SaveTicket;
use App\Domains\Ticket\Constants\CategoryTicket;
use App\Domains\Ticket\Constants\State;
use App\Domains\Ticket\DataTransferObjects\TicketData;
use App\Domains\Ticket\Models\Ticket;
use App\Domains\Ticket\Requests\SaveTicketRequest;
use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractAPIController;
use App\Support\Exceptions\JsonResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class TicketController extends AbstractAPIController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::BROWSE_TICKETS))->only(['index']);
        $this->middleware(sprintf('permission:%s', Permission::READ_TICKET))->only(['show']);
        $this->middleware(sprintf('permission:%s', Permission::ADD_TICKET))->only(['store']);
        $this->middleware(sprintf('permission:%s', Permission::EDIT_TICKET))->only(['update']);
        $this->middleware(sprintf('permission:%s', Permission::DELETE_TICKET))->only(['destroy']);
    }

    public function index(Event $event): JsonResponse
    {
        return $this->sendJsonResponse(TicketData::collection($event->ticket()->get()));
    }

    public function store(Ticket $ticket, SaveTicketRequest $request): JsonResponse
    {
        SaveTicket::dispatchSync($ticket, TicketData::from($request));

        return $this->sendJsonResponse([
            'message' => 'stored_data', ['data' => 'ticket'],
        ]);
    }

    public function update(SaveTicketRequest $request, Ticket $ticket): JsonResponse
    {
        SaveTicket::dispatchSync($ticket, TicketData::from($request->all()));

        return $this->sendJsonResponse([
            'message' => 'updated_data', ['data' => 'ticket'],
        ]);
    }

    public function show(Ticket $ticket): JsonResponse
    {
        return $this->sendJsonResponse(
            TicketData::from($ticket)->include(
                'event.user',
            )
        );
    }

    /**
     * @throws JsonResponseException
     */
    public function destroy(Ticket $ticket): JsonResponse
    {
        try {
            $ticket->delete();

            return $this->sendJsonResponse([
                'message' => 'deleted_data', ['data' => 'ticket deleted'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }

    public function state(): JsonResponse
    {
        return $this->sendJsonResponse(State::get()->values()->toArray());
    }

    public function categoryTicket(): JsonResponse
    {
        return $this->sendJsonResponse(CategoryTicket::get()->values()->toArray());
    }
}
