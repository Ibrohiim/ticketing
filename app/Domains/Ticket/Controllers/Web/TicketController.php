<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Controllers\Web;

use App\Domains\Ticket\Models\Ticket;
use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractWebController;
use Illuminate\Contracts\View\View;

class TicketController extends AbstractWebController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::MANAGE_TICKETS));
    }

    public function index(): View
    {
        return view('tickets.index');
    }

    public function show(Ticket $ticket): View
    {
        return view('tickets.show', compact('ticket'));
    }
}
