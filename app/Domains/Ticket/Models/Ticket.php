<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Models;

use App\Domains\Event\Models\Event;
use App\Domains\Promo\Models\Promo;
use App\Domains\Ticket\Exceptions\TicketException;
use App\Domains\Ticket\Scopes\OwnTicketScope;
use App\Support\Models\AbstractModel;
use Database\Factories\TicketFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ticket extends AbstractModel
{
    protected $fillable = [
        'ticket_name',
        // 'ticket_code',
        'ticket_category',
        'quantity',
        'price',
        'sales_start_date',
        'sales_finish_date',
        'description',
        'state'

    ];
    protected $casts = [
        'price' => 'float',
    ];

    protected static function booted(): void
    {
        parent::booted();
        static::addGlobalScope(new OwnTicketScope());
        self::deleting(static function (self $ticket) {
            if ($ticket->promo()->count() > 0) {
                throw TicketException::ticketHasPromo($ticket);
            }
        });
    }

    protected static function newFactory(): TicketFactory
    {
        return TicketFactory::new();
    }

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function promo(): HasMany
    {
        return $this->hasMany(Promo::class);
    }
}
