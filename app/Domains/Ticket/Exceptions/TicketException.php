<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Exceptions;

use App\Domains\Ticket\Models\Ticket;
use App\Support\Exceptions\AbstractException;

class TicketException extends AbstractException
{
    public static function ticketHasPromo(Ticket $ticket): self
    {
        return new self("Delete failed, ticket $ticket->ticket_name has promo");
    }
}
