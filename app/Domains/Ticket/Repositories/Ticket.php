<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Repositories;

use App\Support\Repositories\AbstractRepository;

class Ticket extends AbstractRepository
{
    protected string $model = \App\Domains\Ticket\Models\Ticket::class;
    protected ?array $searchableColumns = ['ticket_name'];
    protected string $defaultSort = 'ticket_name';
    protected string $defaultSortDirection = parent::SORT_DIRECTION_ASC;
    protected ?array $with = ['event'];

    public function event(int $eventId): static
    {
        $this->query = $this->query->where('event_id', $eventId);

        return $this;
    }

    public function user(int $userId): static
    {
        $this->query = $this->query->whereHas('event', function ($query) use ($userId) {
            $query->where('user_id', $userId);
        });

        return $this;
    }
}
