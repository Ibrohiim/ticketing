<?php

declare(strict_types=1);

namespace App\Domains\Ticket\Actions;

use App\Domains\Ticket\Constants\CategoryTicket;
use App\Domains\Ticket\Constants\State;
use App\Domains\Ticket\DataTransferObjects\TicketData;
use App\Domains\Ticket\Models\Ticket;
use App\Support\Actions\AbstractAction;
use Illuminate\Support\Facades\DB;

class SaveTicket extends AbstractAction
{
    public function __construct(
        private readonly Ticket $ticket,
        private readonly TicketData $data
    ) {
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $category = ($this->data->ticket_category === 'paid') ? CategoryTicket::PAID : CategoryTicket::FREE;
            $state = ($this->data->quantity > 0) ? State::AVAILABLE : State::SOLD;

            $this->ticket->event()->associate($this->data->event_id);

            $this->ticket->fill([
                'ticket_name' => $this->data->ticket_name,
                'ticket_category' => $category,
                'quantity' => $this->data->quantity,
                'price' => $this->data->price,
                'sales_start_date' => $this->data->sales_start_date,
                'sales_finish_date' => $this->data->sales_finish_date,
                'description' => $this->data->description,
                'state' => $state
            ])->save();
        });
    }
}
