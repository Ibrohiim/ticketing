<?php

declare(strict_types=1);

namespace App\Domains\Ticket\DataTransferObjects;

use App\Domains\Ticket\Models\Ticket;
use Carbon\Carbon;
use Spatie\LaravelData\Data;

class TicketData extends Data
{
    public function __construct(
        public ?int $id,
        public int $event_id,
        public string $ticket_name,
        public string $ticket_category,
        public ?string $ticket_category_formatted,
        public string $quantity,
        public float $price,
        public string $sales_start_date,
        public ?string $sales_start_date_formatted,
        public string $sales_finish_date,
        public ?string $sales_finish_date_formatted,
        public ?string $description,
        public ?string $state,
        public ?string $state_formatted,
    ) {
    }

    public static function fromModel(Ticket $ticket): self
    {
        return new self(
            id: $ticket->id,
            event_id: $ticket->event_id,
            ticket_name: $ticket->ticket_name,
            ticket_category: $ticket->ticket_category,
            ticket_category_formatted: self::ticketCategoryFormatted($ticket),
            quantity: $ticket->quantity,
            price: $ticket->price,
            sales_start_date: $ticket->sales_start_date,
            sales_start_date_formatted: Carbon::parse($ticket->sales_start_date)->format(config('rnd.human_date_time_format')),
            sales_finish_date: $ticket->sales_finish_date,
            sales_finish_date_formatted: Carbon::parse($ticket->sales_finish_date)->format(config('rnd.human_date_time_format')),
            description: $ticket->description,
            state: $ticket->state,
            state_formatted: self::stateFormatted($ticket),
        );
    }

    private static function stateFormatted(Ticket $ticket): string
    {
        $stateFormatted = '&ndash;';
        if ($ticket->state) {
            $stateFormatted = state_badge(
                $ticket->state,
                $ticket->state
            );
        }

        return $stateFormatted;
    }

    private static function ticketCategoryFormatted(Ticket $ticket): string
    {
        $categoryFormatted = '&ndash;';
        if ($ticket->ticket_category) {
            $categoryFormatted = state_badge(
                $ticket->ticket_category,
                $ticket->ticket_category
            );
        }

        return $categoryFormatted;
    }
}
