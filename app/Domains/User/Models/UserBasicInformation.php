<?php

declare(strict_types=1);

namespace App\Domains\User\Models;

use App\Domains\User\Scopes\OwnScope;
use App\Support\Models\AbstractModel;
use Database\Factories\UserBasicInformationFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class UserBasicInformation extends AbstractModel
{
    protected $table = 'user_basic_informations';

    protected $fillable = [
        'organizer_name',
        'phone',
        'address',
        'about',
        'featured_event',
        'twitter',
        'instagram',
        'facebook',
        'profile_link',
        'profile_image'
    ];

    protected static function booted(): void
    {
        parent::booted();
        static::addGlobalScope(new OwnScope());
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    protected static function newFactory(): UserBasicInformationFactory
    {
        return UserBasicInformationFactory::new();
    }

    public function profileImage(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset(Storage::url($value ? $value : 'profile/noimage.png')),
        );
    }
}
