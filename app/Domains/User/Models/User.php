<?php

declare(strict_types=1);

namespace App\Domains\User\Models;

use App\Domains\User\Exceptions\UserException;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'password',
        'firstname',
        'lastname',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot(): void
    {
        parent::boot();
        static::creating(static function (self $user) {
            $user->password = bcrypt($user->password);
        });

        static::deleting(static function (self $user) {
            if (auth()->id() === $user->id) {
                throw UserException::cannotDeleteSelf();
            }
        });
    }

    protected static function newFactory(): UserFactory
    {
        return UserFactory::new();
    }

    public function roleKey(): Attribute
    {
        return Attribute::get(fn () => $this->getRoleNames()->first());
    }

    public function roleName(): Attribute
    {
        return Attribute::get(fn () => $this->getRoleNames()->first());
    }

    public function userBasicInformation(): HasOne
    {
        return $this->hasOne(UserBasicInformation::class)->orderBy('organizer_name');
    }

    public function userLegalInformation(): HasOne
    {
        return $this->hasOne(UserLegalInformation::class)->orderBy('ktp_name');
    }

    public function banking(): HasOne
    {
        return $this->hasOne(Banking::class)->orderBy('owner_name');
    }
}
