<?php

declare(strict_types=1);

namespace App\Domains\User\Models;

use App\Domains\User\Scopes\OwnScope;
use App\Support\Models\AbstractModel;
use Database\Factories\BankingFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Banking extends AbstractModel
{
    use HasUserTrait;
    use HasFactory;
    use HasRoles;
    use SoftDeletes;

    protected $table = 'banks';
    protected $fillable = [
        'bank_name',
        'owner_name',
        'account_number',
        'branch',
        'city',
    ];

    protected static function booted(): void
    {
        parent::booted();
        static::addGlobalScope(new OwnScope());
    }

    protected static function newFactory(): BankingFactory
    {
        return BankingFactory::new();
    }
}
