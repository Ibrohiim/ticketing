<?php

declare(strict_types=1);

namespace App\Domains\User\Models;

use App\Domains\User\Scopes\OwnScope;
use App\Support\Models\AbstractModel;
use Database\Factories\UserLegalInformationFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class UserLegalInformation extends AbstractModel
{
    // use HasUserTrait;
    // use HasFactory;
    // use HasRoles;
    // use SoftDeletes;

    protected $table = 'user_legal_informations';
    protected $fillable = [
        'ktp_number',
        'ktp_name',
        'ktp_address',
        'npwp_number',
        'npwp_name',
        'npwp_address',
    ];

    protected static function booted(): void
    {
        parent::booted();
        static::addGlobalScope(new OwnScope());
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    protected static function newFactory(): UserLegalInformationFactory
    {
        return UserLegalInformationFactory::new();
    }
}
