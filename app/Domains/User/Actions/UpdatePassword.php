<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\User\DataTransferObjects\UpdatePasswordData;
use App\Domains\User\Models\User;
use App\Support\Actions\AbstractAction;
use Illuminate\Contracts\Auth\Authenticatable;

class UpdatePassword extends AbstractAction
{
    public function __construct(
        private readonly Authenticatable|User $user,
        private readonly UpdatePasswordData $data
    ) {
    }

    public function handle(): void
    {
        $this->user->fill(['password' => bcrypt($this->data->password)])->save();
    }
}
