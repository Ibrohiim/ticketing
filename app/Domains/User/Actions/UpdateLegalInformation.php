<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\User\DataTransferObjects\UpdateLegalInformationData;
use App\Domains\User\Models\User;
use App\Domains\User\Models\UserLegalInformation;
use App\Support\Actions\AbstractAction;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;

class UpdateLegalInformation extends AbstractAction
{
    public function __construct(
        private readonly User|Authenticatable $user,
        private readonly UpdateLegalInformationData $data
    ) {
    }

    public function handle(): void
    {
        DB::transaction(function () {
            $legalInformation =  $this->user->userLegalInformation;

            if (!$legalInformation) {
                $legalInformation = new UserLegalInformation();
                $legalInformation->user()->associate($this->data->id);
            }

            $legalInformation->fill([
                'ktp_number' => $this->data->ktp_number,
                'ktp_name' => $this->data->ktp_name,
                'ktp_address' => $this->data->ktp_address,
                'npwp_number' => $this->data->npwp_number,
                'npwp_name' => $this->data->npwp_name,
                'npwp_address' => $this->data->npwp_address,
            ])->save();
        });
    }
}
