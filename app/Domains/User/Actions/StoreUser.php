<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\User\DataTransferObjects\SaveUserData;
use App\Domains\User\Models\User;
use App\Support\Actions\AbstractAction;
use Illuminate\Support\Facades\DB;
use Throwable;

class StoreUser extends AbstractAction
{
    public function __construct(
        private readonly SaveUserData $data,
    ) {
    }

    /**
     * @throws Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $user = new User();
            $user->fill($this->data->toArray());
            $user->save();
            $user->syncRoles($this->data->role);
        });
    }
}
