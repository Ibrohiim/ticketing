<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\User\DataTransferObjects\SaveUserData;
use App\Domains\User\Models\User;
use App\Support\Actions\AbstractAction;
use Illuminate\Support\Facades\DB;
use Throwable;

class UpdateUser extends AbstractAction
{
    public function __construct(private readonly User $user, private readonly SaveUserData $data)
    {
    }

    /**
     * @throws Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $this->user->is_blocked = $this->data->is_blocked;
            $this->user->fill([
                'email' => $this->data->email,
                'firstname' => $this->data->firstname,
                'lastname' => $this->data->lastname,
            ]);

            $this->user->save();
            $this->user->syncRoles($this->data->role);
        });
    }
}
