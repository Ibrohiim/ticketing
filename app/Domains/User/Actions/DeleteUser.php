<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\Staff\Exceptions\StaffException;
use App\Domains\User\Constants\Role;
use App\Domains\User\Models\User;
use App\Support\Actions\AbstractAction;
use Illuminate\Support\Facades\DB;
use Throwable;

class DeleteUser extends AbstractAction
{
    public function __construct(private readonly User $user)
    {
    }

    /**
     * @throws Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $this->user->delete();
        });
    }
}
