<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\User\Exceptions\UserException;
use App\Domains\User\Models\User;
use App\Domains\User\Models\UserBasicInformation;
use App\Support\Actions\AbstractAction;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UpdateImage extends AbstractAction
{
    public function __construct(
        private readonly User|Authenticatable $user,
        protected $image,
    ) {
    }

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $basicInformation =  $this->user->userBasicInformation;

            if (!$this->image instanceof UploadedFile) {
                throw UserException::invalidFile();
            }

            if (!$basicInformation) {
                $basicInformation = new UserBasicInformation();
                $basicInformation->user()->associate($this->user->id);
            }

            if ($basicInformation) {
                if ($basicInformation->profile_image !== asset(Storage::url('profile/noimage.png'))) {
                    $previousPath = $this->user->userBasicInformation->getRawOriginal('profile_image');
                    Storage::delete($previousPath);
                }
            }

            $link = Storage::put('/public/profile', $this->image);

            $basicInformation->fill([
                'profile_image' => $link,
            ])->save();
        });
    }
}
