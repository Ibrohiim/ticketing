<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\User\DataTransferObjects\UpdateProfileData;
use App\Domains\User\Models\User;
use App\Domains\User\Models\UserBasicInformation;
use App\Support\Actions\AbstractAction;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;

class UpdateProfile extends AbstractAction
{
    public function __construct(
        private readonly User|Authenticatable $user,
        private readonly UpdateProfileData $data
    ) {
    }

    public function handle(): void
    {
        DB::transaction(function () {
            $basicInformation =  $this->user->userBasicInformation;

            if (!$basicInformation) {
                $basicInformation = new UserBasicInformation();
                $basicInformation->user()->associate($this->data->id);
            }

            $basicInformation->fill([
                'organizer_name' => $this->data->organizer_name,
                'phone' => $this->data->phone,
                'address' => $this->data->address,
                'about' => $this->data->about,
                'featured_event' => $this->data->featured_event,
                'instagram' => $this->data->instagram,
                'twitter' => $this->data->twitter,
                'facebook' => $this->data->facebook,
                'profile_link' => $this->data->profile_link,
            ])->save();

            $this->user->fill([
                'firstname' => $this->data->firstname,
                'lastname' => $this->data->lastname,
                'email' => $this->data->email
            ])->save();
        });
    }
}
