<?php

declare(strict_types=1);

namespace App\Domains\User\Actions;

use App\Domains\User\DataTransferObjects\BankingData;
use App\Domains\User\DataTransferObjects\UpdateBankData;
use App\Domains\User\Models\Banking;
use App\Domains\User\Models\User;
use App\Support\Actions\AbstractAction;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;

class UpdateBank extends AbstractAction
{
    public function __construct(
        private readonly User|Authenticatable $user,
        private readonly UpdateBankData $data
    ) {
    }

    public function handle(): void
    {
        DB::transaction(function () {
            $banking =  $this->user->banking;

            if (!$banking) {
                $banking = new Banking();
                $banking->user()->associate($this->data->id);
            }

            $banking->fill([
                'bank_name' => $this->data->bank_name,
                'owner_name' => $this->data->owner_name,
                'account_number' => $this->data->account_number,
                'branch' => $this->data->branch,
                'city' => $this->data->city,
            ])->save();
        });
    }
}
