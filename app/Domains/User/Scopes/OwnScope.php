<?php

declare(strict_types=1);

namespace App\Domains\User\Scopes;

use App\Domains\User\Constants\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class OwnScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        /** @var \App\Domains\User\Models\User|null $authenticatable */
        $authenticatable = auth()->user();

        if ($authenticatable) {
            if ($authenticatable->hasRole(Role::USER)) {
                $builder->where('user_id', $authenticatable->id);
            }
        }
    }
}
