<?php

declare(strict_types=1);

namespace App\Domains\User\Exceptions;

use App\Support\Exceptions\AbstractException;

class UserException extends AbstractException
{
    public static function cannotDeleteSelf(): self
    {
        return new self('Cannot delete self');
    }

    public static function invalidFile(): self
    {
        return new self("Invalid File");
    }
}
