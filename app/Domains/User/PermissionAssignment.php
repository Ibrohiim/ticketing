<?php

declare(strict_types=1);

namespace App\Domains\User;

use App\Domains\User\Constants\Permission;
use App\Domains\User\Constants\Role;

class PermissionAssignment
{
    public static function assignment(): array
    {
        return [
            // dashboard
            Permission::VIEW_DASHBOARD => [
                Role::ADMINISTRATOR,
                Role::USER,
            ],

            // master data
            Permission::VIEW_MENU_MASTERDATA => [
                Role::ADMINISTRATOR,
            ],

            // user
            Permission::MANAGE_USERS => [Role::ADMINISTRATOR],
            Permission::BROWSE_USERS => [Role::ADMINISTRATOR],
            Permission::READ_USER => [Role::ADMINISTRATOR],
            Permission::EDIT_USER => [Role::ADMINISTRATOR],
            Permission::ADD_USER => [Role::ADMINISTRATOR],
            Permission::DELETE_USER => [Role::ADMINISTRATOR],

            // profile
            Permission::MANAGE_PROFILES => [Role::ADMINISTRATOR, Role::USER],
            Permission::BROWSE_PROFILES => [Role::ADMINISTRATOR, Role::USER],
            Permission::READ_PROFILE => [Role::ADMINISTRATOR, Role::USER],
            Permission::EDIT_PROFILE => [Role::ADMINISTRATOR], Role::USER,

            // event format
            Permission::MANAGE_EVENT_FORMATS => [Role::ADMINISTRATOR],
            Permission::BROWSE_EVENT_FORMATS => [Role::ADMINISTRATOR],
            Permission::READ_EVENT_FORMAT => [Role::ADMINISTRATOR],
            Permission::EDIT_EVENT_FORMAT => [Role::ADMINISTRATOR],
            Permission::ADD_EVENT_FORMAT => [Role::ADMINISTRATOR],
            Permission::DELETE_EVENT_FORMAT => [Role::ADMINISTRATOR],

            // event category
            Permission::MANAGE_EVENT_CATEGORIES => [Role::ADMINISTRATOR],
            Permission::BROWSE_EVENT_CATEGORIES => [Role::ADMINISTRATOR],
            Permission::READ_EVENT_CATEGORY => [Role::ADMINISTRATOR],
            Permission::EDIT_EVENT_CATEGORY => [Role::ADMINISTRATOR],
            Permission::ADD_EVENT_CATEGORY => [Role::ADMINISTRATOR],
            Permission::DELETE_EVENT_CATEGORY => [Role::ADMINISTRATOR],

            // events
            Permission::MANAGE_EVENTS => [Role::ADMINISTRATOR, Role::USER],
            Permission::BROWSE_EVENTS => [Role::ADMINISTRATOR, Role::USER],
            Permission::READ_EVENT => [Role::ADMINISTRATOR, Role::USER],
            Permission::EDIT_EVENT => [Role::ADMINISTRATOR, Role::USER],
            Permission::ADD_EVENT => [Role::ADMINISTRATOR, Role::USER],
            Permission::DELETE_EVENT => [Role::ADMINISTRATOR, Role::USER],

            // tickets
            Permission::MANAGE_TICKETS => [Role::ADMINISTRATOR, Role::USER],
            Permission::BROWSE_TICKETS => [Role::ADMINISTRATOR, Role::USER],
            Permission::READ_TICKET => [Role::ADMINISTRATOR, Role::USER],
            Permission::EDIT_TICKET => [Role::ADMINISTRATOR, Role::USER],
            Permission::ADD_TICKET => [Role::ADMINISTRATOR, Role::USER],
            Permission::DELETE_TICKET => [Role::ADMINISTRATOR, Role::USER],

            // ticket_discounts
            Permission::MANAGE_PROMO => [Role::ADMINISTRATOR, Role::USER],
            Permission::BROWSE_PROMO => [Role::ADMINISTRATOR, Role::USER],
            Permission::READ_PROMO => [Role::ADMINISTRATOR, Role::USER],
            Permission::EDIT_PROMO => [Role::ADMINISTRATOR, Role::USER],
            Permission::ADD_PROMO => [Role::ADMINISTRATOR, Role::USER],
            Permission::DELETE_PROMO => [Role::ADMINISTRATOR, Role::USER],

            // acount
            Permission::MANAGE_ACCOUNTS => [Role::ADMINISTRATOR],
            Permission::BROWSE_ACCOUNTS => [Role::ADMINISTRATOR, Role::USER],
            Permission::READ_ACCOUNT => [Role::ADMINISTRATOR, Role::USER],
            Permission::EDIT_ACCOUNT => [Role::ADMINISTRATOR, Role::USER],
            Permission::ADD_ACCOUNT => [Role::ADMINISTRATOR, Role::USER],
            Permission::DELETE_ACCOUNT => [Role::ADMINISTRATOR, Role::USER],
        ];
    }
}
