<?php

declare(strict_types=1);

namespace App\Domains\User\Fortify;

use App\Domains\User\Constants\Role;
use App\Providers\RouteServiceProvider;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;
use Laravel\Fortify\Fortify;
use Symfony\Component\HttpFoundation\Response;

class LoginResponse implements LoginResponseContract
{
    /**
     * {@inheritDoc}
     */
    public function toResponse($request): Redirector|Response|RedirectResponse|Application
    {
        info('halo');
        if ($request->user()->is_blocked) {
            auth()->guard()->logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();

            return redirect(Fortify::redirects('logout', '/'));
        }

        // $redirect = $request->user()->hasRole(Role::ADMINISTRATOR)
        //     ? RouteServiceProvider::DASHBOARD
        //     : RouteServiceProvider::HOMEPAGE;

        return response(auth()->user());
    }
}
