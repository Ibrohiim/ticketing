<?php

declare(strict_types=1);

namespace App\Domains\User\Requests;

use App\Domains\User\Constants\Permission;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->hasPermissionTo(Permission::ADD_USER);
    }

    public function rules(): array
    {
        return [
            'password' => ['required', 'confirmed', 'min:8'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->withoutTrashed()],
            'role' => ['required'],
            'firstname' => ['required'],
        ];
    }
}
