<?php

declare(strict_types=1);

namespace App\Domains\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateImageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'profile_image' => ['required', 'file', 'mimes:jpg,jpeg,png'],
        ];
    }
}
