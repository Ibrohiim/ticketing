<?php

declare(strict_types=1);

namespace App\Domains\User\Requests;

use App\Domains\User\Constants\Permission;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->hasPermissionTo(Permission::EDIT_USER);
    }

    public function rules(): array
    {
        $user = $this->route()?->parameter('user');

        return [
            'email' => ['required', 'email', Rule::unique('users', 'email')->withoutTrashed()->ignore($user)],
            'role' => ['required'],
            'firstname' => ['required'],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'is_blocked' => $this->boolean('is_blocked'),
        ]);
    }
}
