<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Spatie\LaravelData\Data;

class UpdateBankData extends Data
{
    public function __construct(
        public int $id,
        public string $bank_name,
        public string $owner_name,
        public int $account_number,
        public string $branch,
        public string $city,
    ) {
    }

    public static function rules(): array
    {
        return [
            'bank_name' => ['required'],
            'owner_name' => ['required'],
            'account_number' => ['required'],
            'branch' => ['required'],
            'city' => ['required'],
        ];
    }
}
