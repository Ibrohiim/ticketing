<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Spatie\LaravelData\Data;

class UpdatePasswordData extends Data
{
    public function __construct(public string $password)
    {
    }

    public static function rules(): array
    {
        return [
            'password' => ['required', 'min:8', 'confirmed'],
            'password_confirmation' => ['required:password', 'same:password'],
        ];
    }
}
