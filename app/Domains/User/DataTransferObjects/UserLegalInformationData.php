<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Spatie\LaravelData\Data;

class UserLegalInformationData extends Data
{
    public function __construct(
        public ?int $id,
        public int $user_id,
        public ?int $ktp_number,
        public ?string $ktp_name,
        public ?string $ktp_address,
        public ?int $npwp_number,
        public ?string $npwp_name,
        public ?string $npwp_address,
    ) {
    }
}
