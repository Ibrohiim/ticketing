<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use App\Domains\User\Models\User;
use App\Domains\User\Models\UserBasicInformation;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Lazy;

class ProfileData extends Data
{
    public function __construct(
        public int $id,
        public string $firstname,
        public ?string $lastname,
        public string $email,
        public ?string $role,
        public ?string $role_name,
        public ?array $permissions,
        public Lazy|UserBasicInformationData|null $basic_information,
        public Lazy|UserLegalInformationData|null $legal_information,
        public Lazy|BankingData|null $banking,
    ) {
    }

    public static function fromModel(User $user): self
    {
        $permissions = $user->getPermissionsViaRoles()
            ->map(static fn ($permission) => $permission->name)
            ->values()
            ->all();

        return new self(
            id: $user->id,
            firstname: $user->firstname,
            lastname: $user->lastname,
            email: $user->email,
            role: $user->role_key,
            role_name: $user->role_name,
            permissions: $permissions,
            basic_information: $user->userBasicInformation ? Lazy::create(static fn () => UserBasicInformationData::from($user->userBasicInformation)) : new UserBasicInformationData(
                id: null,
                user_id: $user->id,
                organizer_name: null,
                phone: null,
                address: null,
                about: null,
                featured_event: null,
                instagram: null,
                twitter: null,
                facebook: null,
                profile_link: null,
                profile_image: asset(Storage::url('profile/noimage.png')),
            ),
            legal_information: $user->userLegalInformation ? Lazy::create(static fn () => UserLegalInformationData::from($user->userLegalInformation)) : new UserLegalInformationData(
                id: null,
                user_id: $user->id,
                ktp_number: null,
                ktp_name: null,
                ktp_address: null,
                npwp_number: null,
                npwp_name: null,
                npwp_address: null,
            ),
            banking: $user->banking ? Lazy::create(static fn () => BankingData::from($user->banking)) : new BankingData(
                id: null,
                user_id: $user->id,
                bank_name: null,
                owner_name: null,
                account_number: null,
                branch: null,
                city: null,
            ),
        );
    }
}
