<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class UpdateProfileData extends Data
{
    public function __construct(
        public int $id,
        public string $email,
        public string $firstname,
        public ?string $lastname,
        public ?string $organizer_name,
        public ?string $phone,
        public ?string $address,
        public ?string $about,
        public ?string $featured_event,
        public ?string $instagram,
        public ?string $twitter,
        public ?string $facebook,
        public ?string $profile_link,
    ) {
    }

    public static function rules(): array
    {
        return [
            'firstname' => ['required'],
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore(auth()->id())->withoutTrashed(),
            ],
        ];
    }
}
