<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Spatie\LaravelData\Data;

class UserBasicInformationData extends Data
{
    public function __construct(
        public ?int $id,
        public int $user_id,
        public ?string $organizer_name,
        public ?string $phone,
        public ?string $address,
        public ?string $about,
        public ?string $featured_event,
        public ?string $instagram,
        public ?string $twitter,
        public ?string $facebook,
        public ?string $profile_link,
        public ?string $profile_image,
    ) {
    }
}
