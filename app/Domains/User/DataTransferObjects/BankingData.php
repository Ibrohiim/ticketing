<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Spatie\LaravelData\Data;

class BankingData extends Data
{
    public function __construct(
        public ?int $id,
        public int $user_id,
        public ?string $bank_name,
        public ?string $owner_name,
        public ?int $account_number,
        public ?string $branch,
        public ?string $city,
    ) {
    }
}
