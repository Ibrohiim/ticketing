<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Spatie\LaravelData\Data;

class SaveUserData extends Data
{
    public function __construct(
        public string $email,
        public ?string $password,
        public string $firstname,
        public ?string $lastname,
        public ?bool $is_blocked,
        public string $role,
    ) {
    }
}
