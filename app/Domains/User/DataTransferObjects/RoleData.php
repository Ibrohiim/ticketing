<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use Spatie\LaravelData\Data;

class RoleData extends Data
{
    public function __construct(
        public string $key,
        public string $name,
    ) {
    }
}
