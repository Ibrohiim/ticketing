<?php

declare(strict_types=1);

namespace App\Domains\User\DataTransferObjects;

use App\Domains\User\Models\User;
use Spatie\LaravelData\Data;

class UserData extends Data
{
    public function __construct(
        public int $id,
        public string $email,
        public string $firstname,
        public ?string $lastname,
        public bool $is_blocked,
        public ?string $role,
        public ?string $role_name,
    ) {
    }

    public static function fromModel(User $user): self
    {
        return new self(
            $user->id,
            $user->email,
            $user->firstname,
            $user->lastname,
            $user->is_blocked,
            $user->role_key,
            $user->role_name,
        );
    }
}
