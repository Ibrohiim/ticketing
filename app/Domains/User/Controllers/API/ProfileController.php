<?php

declare(strict_types=1);

namespace App\Domains\User\Controllers\API;

use App\Domains\User\Actions\UpdateBank;
use App\Domains\User\Actions\UpdateImage;
use App\Domains\User\Actions\UpdateLegalInformation;
use App\Domains\User\Actions\UpdatePassword;
use App\Domains\User\Actions\UpdateProfile;
use App\Domains\User\DataTransferObjects\BankingData;
use App\Domains\User\DataTransferObjects\ProfileData;
use App\Domains\User\DataTransferObjects\UpdateBankData;
use App\Domains\User\DataTransferObjects\UpdateLegalInformationData;
use App\Domains\User\DataTransferObjects\UpdatePasswordData;
use App\Domains\User\DataTransferObjects\UpdateProfileData;
use App\Domains\User\Requests\UpdateImageRequest;
use App\Support\Controllers\AbstractAPIController;
use Illuminate\Http\JsonResponse;
use Throwable;

class ProfileController extends AbstractAPIController
{
    public function index(): JsonResponse
    {
        return $this->sendJsonResponse(ProfileData::from(auth()->user())->include('basic_information', 'legal_information', 'banking'));
    }

    public function update(UpdateProfileData $data): JsonResponse
    {
        UpdateProfile::dispatchSync(auth()->user(), $data);

        return $this->sendJsonResponse([
            'message' => 'updated_data', ['data' => 'profile'],
        ]);
    }

    public function updateLegalInformation(UpdateLegalInformationData $data): JsonResponse
    {
        UpdateLegalInformation::dispatchSync(auth()->user(), $data);

        return $this->sendJsonResponse([
            'message' => 'Data updated', ['data' => 'profile'],
        ]);
    }

    public function updateBank(UpdateBankData $data): JsonResponse
    {
        UpdateBank::dispatchSync(auth()->user(), $data);

        return $this->sendJsonResponse([
            'message' => 'Data updated', ['data' => 'profile'],
        ]);
    }

    public function updatePassword(UpdatePasswordData $data): JsonResponse
    {
        UpdatePassword::dispatchSync(auth()->user(), $data);

        return $this->sendJsonResponse([
            'message' => __('app.updated_data', ['data' => __('app.password')]),
        ]);
    }

    public function updateProfileImage(UpdateImageRequest $request): JsonResponse
    {
        UpdateImage::dispatchSync(auth()->user(), $request->file('profile_image'));

        return $this->sendJsonResponse([
            'message' => 'Profile picture uploaded successfully!', ['data' => 'image'],
        ]);
    }
}
