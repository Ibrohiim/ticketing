<?php

declare(strict_types=1);

namespace App\Domains\User\Controllers\API;

use App\Domains\User\Constants\Permission;
use App\Domains\User\Constants\Role;
use App\Domains\User\DataTransferObjects\RoleData;
use App\Support\Controllers\AbstractAPIController;
use Illuminate\Http\JsonResponse;

class UserRoleController extends AbstractAPIController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::MANAGE_USERS))->only(['index']);
    }

    public function __invoke(): JsonResponse
    {
        $roles = Role::get()->map(static function ($role) {
            return [
                'key' => $role,
                'name' => $role,
            ];
        })->values();

        return $this->sendJsonResponse(RoleData::collection($roles));
    }
}
