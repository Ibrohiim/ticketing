<?php

declare(strict_types=1);

namespace App\Domains\User\Controllers\API;

use App\Domains\User\Actions\DeleteUser;
use App\Domains\User\Actions\StoreUser;
use App\Domains\User\Actions\UpdateUser;
use App\Domains\User\Constants\Permission;
use App\Domains\User\DataTransferObjects\SaveUserData;
use App\Domains\User\DataTransferObjects\UserData;
use App\Domains\User\Models\User;
use App\Domains\User\Requests\StoreUserRequest;
use App\Domains\User\Requests\UpdateUserRequest;
use App\Support\Controllers\AbstractAPIController;
use App\Support\Exceptions\JsonResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class UserController extends AbstractAPIController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::BROWSE_USERS))->only(['index']);
        $this->middleware(sprintf('permission:%s', Permission::ADD_USER))->only(['store']);
        $this->middleware(sprintf('permission:%s', Permission::EDIT_USER))->only(['update']);
        $this->middleware(sprintf('permission:%s', Permission::DELETE_USER))->only(['destroy']);
    }

    public function index(Request $request): JsonResponse
    {
        $repository = new \App\Domains\User\Repositories\User();

        if ($request->filled('role')) {
            $repository->role($request->input('role'));
        }

        if ($request->filled('q')) {
            $repository->search($request->input('q'));
        }

        $users = $request->boolean('paginate') ? $repository->paginate($request->all()) : $repository->get();

        return $this->sendJsonResponse(UserData::collection($users));
    }

    /**
     * @throws \App\Support\Exceptions\JsonResponseException
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        try {
            StoreUser::dispatchSync(SaveUserData::from($request->input()));

            return $this->sendJsonResponse([
                'message' => 'User saved!', ['data' => 'user'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }

    /**
     * @throws \App\Support\Exceptions\JsonResponseException
     */
    public function update(UpdateUserRequest $request, User $user): JsonResponse
    {
        try {
            UpdateUser::dispatchSync($user, SaveUserData::from($request->input()));

            return $this->sendJsonResponse([
                'message' => 'User updated!', ['data' => 'user'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }

    /**
     * @throws JsonResponseException
     */
    public function destroy(User $user): JsonResponse
    {
        try {
            DeleteUser::dispatchSync($user);

            return $this->sendJsonResponse([
                'message' => 'User Deleted', ['data' => 'user'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }
}
