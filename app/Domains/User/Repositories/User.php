<?php

declare(strict_types=1);

namespace App\Domains\User\Repositories;

use App\Support\Repositories\AbstractRepository;

class User extends AbstractRepository
{
    protected string $model = \App\Domains\User\Models\User::class;

    protected ?array $searchableColumns = ['email'];

    protected string $defaultSort = 'email';

    protected string $defaultSortDirection = parent::SORT_DIRECTION_ASC;

    public function role(string $role): static
    {
        $this->query = $this->query->whereHas('roles', static function ($query) use ($role) {
            $query->where('name', $role);
        });

        return $this;
    }
}
