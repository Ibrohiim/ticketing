<?php

declare(strict_types=1);

namespace App\Domains\User\Constants;

use App\Support\Constants\AbstractConstant;

class Role extends AbstractConstant
{
    public const ADMINISTRATOR = 'administrator';
    public const USER = 'user';
}
