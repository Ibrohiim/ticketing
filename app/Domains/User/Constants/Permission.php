<?php

declare(strict_types=1);

namespace App\Domains\User\Constants;

use App\Support\Constants\AbstractConstant;

class Permission extends AbstractConstant
{
    public const VIEW_DASHBOARD = 'view_dashboard';
    public const VIEW_MENU_MASTERDATA = 'view_menu_masterdata';

    // users
    public const MANAGE_USERS = 'manage_users';
    public const BROWSE_USERS = 'browse_users';
    public const READ_USER = 'read_user';
    public const EDIT_USER = 'edit_user';
    public const ADD_USER = 'add_user';
    public const DELETE_USER = 'delete_user';

    // profile
    public const MANAGE_PROFILES = 'manage_profiles';
    public const BROWSE_PROFILES = 'browse_profiles';
    public const READ_PROFILE = 'read_profile';
    public const EDIT_PROFILE = 'edit_profile';

    // event formats
    public const MANAGE_EVENT_FORMATS = 'manage_event_formats';
    public const BROWSE_EVENT_FORMATS = 'browse_event_formats';
    public const READ_EVENT_FORMAT = 'read_event_format';
    public const EDIT_EVENT_FORMAT = 'edit_event_format';
    public const ADD_EVENT_FORMAT = 'add_event_format';
    public const DELETE_EVENT_FORMAT = 'delete_event_format';

    // event categories
    public const MANAGE_EVENT_CATEGORIES = 'manage_event_categories';
    public const BROWSE_EVENT_CATEGORIES = 'browse_event_categories';
    public const READ_EVENT_CATEGORY = 'read_event_category';
    public const EDIT_EVENT_CATEGORY = 'edit_event_category';
    public const ADD_EVENT_CATEGORY = 'add_event_category';
    public const DELETE_EVENT_CATEGORY = 'delete_event_category';

    // events
    public const MANAGE_EVENTS = 'manage_events';
    public const BROWSE_EVENTS = 'browse_events';
    public const READ_EVENT = 'read_event';
    public const EDIT_EVENT = 'edit_event';
    public const ADD_EVENT = 'add_event';
    public const DELETE_EVENT = 'delete_event';

    // event tickets
    public const MANAGE_TICKETS = 'manage_tickets';
    public const BROWSE_TICKETS = 'browse_tickets';
    public const READ_TICKET = 'read_ticket';
    public const EDIT_TICKET = 'edit_ticket';
    public const ADD_TICKET = 'add_ticket';
    public const DELETE_TICKET = 'delete_ticket';

    // event promo
    public const MANAGE_PROMO = 'manage_promo';
    public const BROWSE_PROMO = 'browse_promo';
    public const READ_PROMO = 'read_promo';
    public const EDIT_PROMO = 'edit_promo';
    public const ADD_PROMO = 'add_promo';
    public const DELETE_PROMO = 'delete_promo';

    // account
    public const MANAGE_ACCOUNTS = 'manage_accounts';
    public const BROWSE_ACCOUNTS = 'browse_accounts';
    public const READ_ACCOUNT = 'read_account';
    public const EDIT_ACCOUNT = 'edit_account';
    public const ADD_ACCOUNT = 'add_account';
    public const DELETE_ACCOUNT = 'delete_account';
}
