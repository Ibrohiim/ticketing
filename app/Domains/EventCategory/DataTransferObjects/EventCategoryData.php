<?php

declare(strict_types=1);

namespace App\Domains\EventCategory\DataTransferObjects;

use Spatie\LaravelData\Data;

class EventCategoryData extends Data
{
    public function __construct(
        public ?int $id,
        public string $name,
        public ?string $description
    ) {
    }
}
