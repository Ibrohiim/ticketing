<?php

declare(strict_types=1);

namespace App\Domains\EventCategory\Actions;

use App\Domains\EventCategory\DataTransferObjects\EventCategoryData;
use App\Domains\EventCategory\Models\EventCategory;
use App\Support\Actions\AbstractAction;

class SaveEventCategory extends AbstractAction
{
    public function __construct(
        private readonly EventCategory $eventCategory,
        private readonly EventCategoryData $data
    ) {
    }

    public function handle(): void
    {
        $this->eventCategory->fill($this->data->toArray())->save();
    }
}
