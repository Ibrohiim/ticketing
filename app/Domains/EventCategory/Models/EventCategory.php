<?php

declare(strict_types=1);

namespace App\Domains\EventCategory\Models;

use App\Domains\Event\Models\Event;
use App\Support\Models\AbstractModel;
use Database\Factories\EventCategoryFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EventCategory extends AbstractModel
{
    protected $fillable = ['name', 'description'];

    protected static function newFactory(): EventCategoryFactory
    {
        return EventCategoryFactory::new();
    }

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
