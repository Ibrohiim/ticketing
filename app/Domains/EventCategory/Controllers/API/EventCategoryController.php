<?php

declare(strict_types=1);

namespace App\Domains\EventCategory\Controllers\API;

use App\Domains\EventCategory\Actions\SaveEventCategory;
use App\Domains\EventCategory\DataTransferObjects\EventCategoryData;
use App\Domains\EventCategory\Models\EventCategory;
use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractAPIController;
use App\Support\Exceptions\JsonResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class EventCategoryController extends AbstractAPIController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::BROWSE_EVENT_CATEGORIES))->only(['index']);
        $this->middleware(sprintf('permission:%s', Permission::ADD_EVENT_CATEGORY))->only(['store']);
        $this->middleware(sprintf('permission:%s', Permission::EDIT_EVENT_CATEGORY))->only(['update']);
        $this->middleware(sprintf('permission:%s', Permission::DELETE_EVENT_CATEGORY))->only(['destroy']);
    }

    public function index(Request $request): JsonResponse
    {
        $repository = (new \App\Domains\EventCategory\Repositories\EventCategory());

        if ($request->filled('q')) {
            $repository->search($request->input('q'));
        }

        $eventCategories = $request->boolean('paginate') ? $repository->paginate($request->all()) : $repository->get();

        return $this->sendJsonResponse(EventCategoryData::collection($eventCategories));
    }

    public function store(EventCategoryData $data): JsonResponse
    {
        SaveEventCategory::dispatchSync(new EventCategory(), $data);

        return $this->sendJsonResponse([
            'message' => 'stored_data', ['data' => 'event_category'],
        ]);
    }

    public function update(EventCategory $eventCategory, EventCategoryData $data): JsonResponse
    {
        SaveEventCategory::dispatchSync($eventCategory, $data);

        return $this->sendJsonResponse([
            'message' => 'updated_data', ['data' => 'event_category'],
        ]);
    }

    // /**
    //  * @throws JsonResponseException
    //  */
    public function destroy(EventCategory $eventCategory): JsonResponse
    {
        try {
            $eventCategory->delete();

            return $this->sendJsonResponse([
                'message' => 'deleted_data', ['data' => 'event_category'],
            ]);
        } catch (Throwable $throwable) {
            throw new JsonResponseException($throwable->getMessage(), $throwable->getCode());
        }
    }
}
