<?php

declare(strict_types=1);

namespace App\Domains\EventCategory\Controllers\Web;

use App\Domains\User\Constants\Permission;
use App\Support\Controllers\AbstractWebController;
use Illuminate\Contracts\View\View;

class EventCategoryController extends AbstractWebController
{
    public function __construct()
    {
        $this->middleware(sprintf('permission:%s', Permission::MANAGE_EVENT_CATEGORIES));
    }

    public function index(): View
    {
        return view('event-categories.index');
    }
}
