<?php

declare(strict_types=1);

namespace App\Domains\EventCategory\Repositories;

use App\Support\Repositories\AbstractRepository;

class EventCategory extends AbstractRepository
{
    protected string $model = \App\Domains\EventCategory\Models\EventCategory::class;
    protected ?array $searchableColumns = ['name'];
    protected string $defaultSort = 'name';
    protected string $defaultSortDirection = parent::SORT_DIRECTION_ASC;
}
