<?php

declare(strict_types=1);

namespace App\Domains\Homepage\Controllers\API;

use App\Domains\Event\DataTransferObjects\EventData;
use App\Domains\Event\Models\Event;
use App\Support\Controllers\AbstractAPIController;
use Illuminate\Http\JsonResponse;
use Throwable;

class FrontpageController extends AbstractAPIController
{

    public function getEvents(Event $event): JsonResponse
    {
        return $this->sendJsonResponse(EventData::collection($event->get())->include('user', 'event_category', 'event_format'));
    }

    public function show(Event $event): JsonResponse
    {
        return $this->sendJsonResponse(
            EventData::from($event)->include(
                'event_format',
                'event_category',
            )
        );
    }
}
