<?php

namespace App\Http\Controllers;

use App\Support\Controllers\AbstractWebController;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Laravel\Fortify\Fortify;

class ApplicationController extends AbstractWebController
{
    public function __invoke(): View
    {
        return view('app');
    }
}
