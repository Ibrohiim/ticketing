<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Domains\User\Constants\Permission;
use Illuminate\Console\Command;

class GeneratePermissionConstants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:generate-permission-constants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Permission.js';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $path = base_path('resources/js/domains/user/Permission.js');

        $permissions = Permission::get()->toArray();
        $fileContent = "// Generated file, do not manually edit. Use php artisan app:generate-permission-constants instead\n";
        $fileContent .= 'export const Permission = {';
        foreach ($permissions as $key => $value) {
            $fileContent .= "\n  $key: '$value',";
        }
        $fileContent .= "\n};";

        file_put_contents($path, $fileContent);
    }
}
