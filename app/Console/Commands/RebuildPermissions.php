<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RebuildPermissions extends Command
{
    protected $signature = 'app:rebuild-permissions';

    protected $description = 'Rebuild Permissions';

    public function handle(): void
    {
        $this->call('cache:clear');

        $this->call('db:seed', [
            '--class' => 'PermissionSeeder',
            '--force' => true,
        ]);
    }
}
