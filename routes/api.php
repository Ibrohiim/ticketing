<?php

use App\Domains\Event\Controllers\API\EventController;
use App\Domains\Event\Controllers\API\EventSortDataController;
use App\Domains\Event\Controllers\API\EventStateController;
use App\Domains\EventCategory\Controllers\API\EventCategoryController;
use App\Domains\EventFormat\Controllers\API\EventFormatController;
use App\Domains\Homepage\Controllers\API\FrontpageController;
use App\Domains\Promo\Controllers\API\PromoController;
use App\Domains\Ticket\Controllers\API\TicketController;
use App\Domains\User\Controllers\API\ProfileController;
use App\Domains\User\Controllers\API\UserController;
use App\Domains\User\Controllers\API\UserRoleController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {
    Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
    // profile
    Route::prefix('profile')->as('profile.')->group(static function () {
        Route::post('update-image', [ProfileController::class, 'updateProfileImage'])
            ->name('update-image');
    });
    Route::match(
        methods: ['put', 'patch'],
        uri: 'profile',
        action: [ProfileController::class, 'update']
    )->name('profile.update');

    Route::match(
        methods: ['put', 'patch'],
        uri: 'profile/legal-information',
        action: [ProfileController::class, 'updateLegalInformation']
    )->name('profile.legal-information');

    Route::match(
        methods: ['put', 'patch'],
        uri: 'profile/update-banking',
        action: [ProfileController::class, 'updateBank']
    )->name('profile.update-banking');

    Route::match(
        methods: ['put', 'patch'],
        uri: 'profile/update-password',
        action: [ProfileController::class, 'updatePassword']
    )->name('profile.update-password');

    Route::resource('users', UserController::class)->except(['create', 'edit']);
    Route::get('user-roles', UserRoleController::class)->name('user-roles');
    Route::resource('event-formats', EventFormatController::class)->except(['create', 'edit']);
    Route::resource('event-categories', EventCategoryController::class)->except(['create', 'edit']);
    // events
    Route::prefix('events')->as('events.')->group(static function () {
        Route::get('state', [EventController::class, 'state'])
            ->name('state');

        Route::get('sort-data/types', [EventSortDataController::class, 'types'])
            ->name('sort-data.types');

        Route::get('states/types', [EventStateController::class, 'types'])->name('states.types');
        Route::post('{event}/update-banner', [EventController::class, 'updateBanner'])->name('update-banner');

        Route::resource('{event}/tickets', TicketController::class)->except(['create', 'edit']);
        Route::get('{event}/tickets/state', [TicketController::class, 'state'])->name('ticket-state');
        Route::get('{event}/ticket/category-ticket', [TicketController::class, 'categoryTicket'])
            ->name('category-ticket');

        Route::resource('{event}/promo', PromoController::class)->except(['create', 'edit']);
        Route::get('{event}/promo-by-event', [PromoController::class, 'getByEventId'])->name('events.promo');
        Route::get('{event}/promo-type', [PromoController::class, 'promoType'])->name('promo-type');
        Route::get('{event}/promo-state', [PromoController::class, 'state'])->name('promo-state');
    });
    Route::resource('events', EventController::class)->except(['create', 'edit']);
});

Route::get('frontpage', [FrontpageController::class, 'getEvents'])->name('event.frontpage');
