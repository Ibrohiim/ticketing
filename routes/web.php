<?php

use App\Http\Controllers\ApplicationController;
use Illuminate\Support\Facades\Route;

// Route::post('/login', [ApplicationController::class, 'login'])->name('login');
Route::group(['middleware' => ['web', 'auth']], static function () {
});

// Route::get('{view}', ApplicationController::class)->where('view', '(.*)')->middleware('auth');
// Route::get('{view}', ApplicationController::class)->where('view', '(.*)');
Route::view('/{any?}', 'app')->name('app')->where('any', '.*');
