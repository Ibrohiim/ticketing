<?php

use App\Domains\Event\Models\Event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Event::class)->constrained();
            $table->string('ticket_name');
            // $table->string('ticket_code');
            $table->string('ticket_category');
            $table->string('quantity')->nullable();
            $table->double('price')->default(0);
            $table->datetime('sales_start_date');
            $table->datetime('sales_finish_date');
            $table->text('description')->nullable();
            $table->string('state');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
