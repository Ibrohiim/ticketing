<?php

use App\Domains\Event\Constants\State;
use App\Domains\EventCategory\Models\EventCategory;
use App\Domains\EventFormat\Models\EventFormat;
use App\Domains\User\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class)->constrained();
            $table->foreignIdFor(EventFormat::class)->constrained();
            $table->foreignIdFor(EventCategory::class)->constrained();
            $table->string('name');
            $table->datetime('start_date')->nullable();
            $table->datetime('finish_date')->nullable();
            $table->string('place')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->text('description')->nullable();
            $table->text('terms_condition')->nullable();
            $table->string('event_link')->nullable();
            $table->text('banner')->nullable();
            $table->string('type');
            $table->string('state')->default(State::DRAFT);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('events');
    }
};
