<?php

use App\Domains\Ticket\Models\Ticket;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('promo_tickets', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Ticket::class)->constrained();
            $table->string('discount_name');
            $table->string('discount_code');
            $table->double('quota')->default(0);
            $table->string('type');
            $table->double('percentage')->default(0);
            $table->double('nominal')->default(0);
            $table->double('min_transaction')->default(0);
            $table->datetime('period_start_date');
            $table->datetime('period_finish_date');
            $table->string('state');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('promo_tickets');
    }
};
