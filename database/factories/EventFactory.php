<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\Event\Constants\State;
use App\Domains\Event\Models\Event;
use App\Domains\EventCategory\Models\EventCategory;
use App\Domains\EventFormat\Models\EventFormat;
use App\Domains\User\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    protected $model = Event::class;

    public function definition(): array
    {
        return [
            'user_id' => static fn () => User::factory()->create()->id,
            'event_format_id' => static fn () => EventFormat::factory()->create()->id,
            'event_category_id' => static fn () => EventCategory::factory()->create()->id,
            'name' => fake()->words(3, true),
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'start_time' => fake()->time('H:i:s'),
            'finish_time' => fake()->time('H:i:s'),
            'place' => fake()->word(3, true),
            'address' => fake()->address(),
            'city' => fake()->city(),
            'description' => fake()->sentence(),
            'terms_condition' => fake()->sentence(),
            'event_link' => fake()->words(3, true),
            'type' => collect(['public', 'private'])->random(),
            'status' => State::DRAFT,
        ];
    }
}
