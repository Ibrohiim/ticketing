<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\User\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domains\User\Models\User>
 */
class UserFactory extends Factory
{
    protected $model = User::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'email' => fake()->unique()->safeEmail(),
            'password' => 'password',
            'firstname' => fake()->firstName(),
            'lastname' => fake()->lastName(),
            'remember_token' => Str::random(10),
            'is_blocked' => collect([true, false])->random(),
        ];
    }
}
