<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\Promo\Constants\PromoType;
use App\Domains\Promo\Constants\State;
use App\Domains\Promo\Models\Promo;
use App\Domains\Ticket\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PromoFactory extends Factory
{
    protected $model = Promo::class;

    public function definition(): array
    {
        return [
            'ticket_id' => static fn () => Ticket::factory()->create()->id,
            'discount_name' => Str::title(fake()->words(3, true)),
            'discount_code' => sprintf('DISC.%s', str_pad((string) fake()->numberBetween(1, 90), 3, '0', 0)),
            'quota' => fake()->randomFloat(),
            'type' => PromoType::NOMINAL,
            'percentage' => fake()->randomFloat(3, 0, 99),
            'nominal' => fake()->randomFloat(),
            'min_transaction' => fake()->randomFloat(),
            'period_start_date' => Carbon::now(),
            'period_finish_date' => Carbon::now(),
            'state' => State::AVAILABLE,
        ];
    }
}
