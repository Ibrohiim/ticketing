<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\EventCategory\Models\EventCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domains\Category\Models\Category>
 */
class EventCategoryFactory extends Factory
{
    protected $model = EventCategory::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => fake()->unique()->words(2, true),
            'description' => fake()->sentence(),
        ];
    }
}
