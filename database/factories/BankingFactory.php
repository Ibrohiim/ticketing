<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\User\Models\Banking;
use App\Domains\User\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BankingFactory extends Factory
{
    protected $model = Banking::class;

    public function definition(): array
    {
        return [
            'user_id' => static fn () => User::factory()->create()->id,
            'bank_name' => fake()->company(),
            'owner_name' => fake()->name(),
            'account_number' => fake()->unique()->numberBetween(16, 0, 99999999),
            'branch' => fake()->word(),
            'city' => fake()->city(),
        ];
    }
}
