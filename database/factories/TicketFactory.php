<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\Event\Models\Event;
use App\Domains\Ticket\Constants\CategoryTicket;
use App\Domains\Ticket\Constants\State;
use App\Domains\Ticket\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TicketFactory extends Factory
{
    protected $model = Ticket::class;

    public function definition(): array
    {
        return [
            'event_id' => static fn () => Event::factory()->create()->id,
            'ticket_name' => Str::title(fake()->words(3, true)),
            // 'ticket_code' => sprintf('TIX.%s', str_pad((string) fake()->numberBetween(1, 90), 3, '0', 0)),
            'ticket_category' => CategoryTicket::PAID,
            'sales_start_date' => Carbon::now(),
            'sales_finish_date' => Carbon::now(),
            'state' => State::SOLD,
        ];
    }
}
