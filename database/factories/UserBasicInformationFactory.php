<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\User\Models\User;
use App\Domains\User\Models\UserBasicInformation;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserBasicInformationFactory extends Factory
{
    protected $model = UserBasicInformation::class;

    public function definition(): array
    {
        return [
            'user_id' => static fn () => User::factory()->create()->id,
            'organizer_name' => fake()->company(),
            'phone' => fake()->phoneNumber(),
            'address' => fake()->sentence(),
            'about' => fake()->sentence(),
            'featured_event' => fake()->words(3, true),
            'twitter' => fake()->name(),
            'instagram' => fake()->name(),
            'facebook' => fake()->url(),
            'profile_link' => fake()->url(),
        ];
    }
}
