<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\User\Models\User;
use App\Domains\User\Models\UserLegalInformation;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserLegalInformationFactory extends Factory
{
    protected $model = UserLegalInformation::class;

    public function definition(): array
    {
        return [
            'user_id' => static fn () => User::factory()->create()->id,
            'ktp_number' => fake()->unique()->numberBetween(16, 0, 99999999),
            'ktp_name' => fake()->name(),
            'ktp_address' => fake()->company(),
            'npwp_number' => fake()->unique()->numberBetween(16, 0, 99999999),
            'npwp_name' => fake()->name(),
            'npwp_address' => fake()->sentence(),
        ];
    }
}
