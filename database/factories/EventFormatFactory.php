<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domains\EventFormat\Models\EventFormat;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domains\Category\Models\Category>
 */
class EventFormatFactory extends Factory
{
    protected $model = EventFormat::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => fake()->unique()->words(2, true),
            'description' => fake()->sentence(),
        ];
    }
}
