<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domains\EventFormat\Models\EventFormat;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventFormatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @throws \Throwable
     */
    public function run(): void
    {
        DB::transaction(static function () {
            EventFormat::truncate();

            $eventFormats = [
                'Festival',
                'Konser',
                'Talk Show',
            ];

            foreach ($eventFormats as $eventFormat) {
                EventFormat::create(['name' => $eventFormat]);
            }
        });
    }
}
