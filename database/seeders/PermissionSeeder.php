<?php

namespace Database\Seeders;

use App\Domains\User\PermissionAssignment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();

        collect(PermissionAssignment::assignment())
            ->each(fn ($roles, $permission) => $this->savePermission($permission, $roles));
    }

    private function savePermission($index, $roles): void
    {
        $permission = Permission::firstOrCreate(['name' => $index]);

        collect($roles)->each(fn ($role) => $this->givePermission($role, $permission));
    }

    private function givePermission($role, $permission): void
    {
        $role = Role::firstOrCreate(['name' => $role]);
        $role->givePermissionTo($permission->name);
    }
}
