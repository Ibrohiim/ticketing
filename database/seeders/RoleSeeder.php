<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Domains\User\Constants\Role::get()->each(static fn ($role) => Role::firstOrCreate(['name' => $role]));
    }
}
