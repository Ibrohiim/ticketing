<?php

namespace Database\Seeders;

use App\Domains\User\Constants\Role;
use App\Domains\User\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(static function () {
            User::truncate();

            if (app()->environment('production')) {
                $u = new User();
                $u->is_blocked = false;
                $u->fill([
                    'email' => 'administrator@example.com',
                    'password' => 'password',
                    'firstname' => 'administrator',
                ])->save();
                $u->assignRole(Role::ADMINISTRATOR);
            }

            if (!app()->environment('production')) {
                Role::get()->each(static function ($role) {
                    /** @var User $u */
                    $u = User::factory()->create([
                        'email' => sprintf('%s@example.com', $role),
                        'is_blocked' => false,
                    ]);
                    $u->assignRole($role);

                    if (Role::USER === $role) {
                        for ($i = 0; $i < 5; $i++) {
                            /** @var User $u */
                            $u = User::factory()->create([
                                'email' => sprintf('%s_%s@example.com', $role, $i + 1),
                                'is_blocked' => false,
                            ]);

                            $u->assignRole($role);
                        }
                    }
                });
            }
        });
    }
}
