<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domains\EventCategory\Models\EventCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @throws \Throwable
     */
    public function run(): void
    {
        DB::transaction(static function () {
            EventCategory::truncate();

            $eventCategories = [
                'Music',
                'Olahrraga',
                'Marketing',
            ];

            foreach ($eventCategories as $eventCategory) {
                EventCategory::create(['name' => $eventCategory]);
            }
        });
    }
}
