<?php

declare(strict_types=1);

return [
    'human_date_format' => 'd M Y',
    'human_time_format' => 'H:i:s',
    'human_date_time_format' => 'd M Y - H:i:s',
    'currency_symbol' => 'Rp',
];
