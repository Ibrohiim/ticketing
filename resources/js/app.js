import "./bootstrap";

import "../../public/themes/assets/static/js/initTheme.js";
import "../../public/themes/assets/static/js/components/dark.js";
import "../../public/themes/assets/extensions/perfect-scrollbar/perfect-scrollbar.min.js";
import "../../public/themes/assets/compiled/js/app.js";
import "bootstrap/dist/js/bootstrap";
import "bootstrap/dist/css/bootstrap.css";

import { createApp } from "vue";
import { createPinia } from "pinia";
import router from "@/support/Router.js";
import { QuillEditor } from "@vueup/vue-quill";
import "@vueup/vue-quill/dist/vue-quill.snow.css";
import { useAuthStore } from "@/domains/user/stores/authStore";

const pinia = createPinia();
const app = createApp({
    created() {
        const authStore = useAuthStore();
        authStore.getAuthenticatedUser();
    },
});

app.use(router).use(pinia);
app.component("QuillEditor", QuillEditor);

app.mount("#app");
