export function formatMoney(amount, digits = 0) {
    const formatter = new Intl.NumberFormat("id-ID", {
        minimumFractionDigits: digits,
        style: "currency",
        currency: "IDR",
    });

    return formatter.format(amount);
}

export function formatNumber(amount, digits = 0) {
    const formatter = new Intl.NumberFormat("id-ID", {
        minimumFractionDigits: digits,
    });

    return formatter.format(amount);
}

export function parseQueryString(params) {
    return Object.keys(params)
        .filter((key) => null !== params[key])
        .map((key) => key + "=" + params[key])
        .join("&");
}

export function quillOptions() {
    return {
        options: {
            debug: "info",
            modules: {
                toolbar: [
                    "bold",
                    "italic",
                    "underline",
                    "strike",
                    { list: "ordered" },
                    { list: "bullet" },
                    "clean",
                ],
            },
            placeholder: "Write here....",
            readOnly: true,
            theme: "snow",
        },
    };
}

export function paginationStats(total, from = 0, to = 0) {
    let stats = "";

    if (from > 0 && to > 0) {
        stats += `${from} &ndash; ${to} ${"from"} `;
    }

    stats += total;

    return stats;
}
