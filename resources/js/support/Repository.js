import axios from "axios";
import { parseQueryString } from "./helper";

export class Repository {
    resource = null;
    axios = axios.create({
        headers: {
            "Content-Type": "application/json",
            Locale: document.documentElement.lang,
        },
    });

    async paginate(options = {}) {
        try {
            return this.get(options, true);
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async get(options = {}, paginate = false) {
        try {
            options.paginate = paginate;
            let url = this.resource;
            if (Object.keys(options).length > 0) {
                url += "?" + parseQueryString(options);
            }
            const response = await this.axios.get(url);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async show(resourceId) {
        try {
            const response = await this.axios.get(
                `${this.resource}/${resourceId}`
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async store(request) {
        try {
            const response = await this.axios.post(this.resource, request);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async update(resourceId, request) {
        try {
            const response = await this.axios.put(
                `${this.resource}/${resourceId}`,
                request
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async destroy(resourceId) {
        try {
            const response = await this.axios.delete(
                `${this.resource}/${resourceId}`
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }
}
