import ProfileTabs from "@/domains/user/views/account-setting/ProfileTabs.vue";
import BasicInformation from "@/domains/user/views/account-setting/BasicInformation.vue";
import LegalInformation from "@/domains/user/views/account-setting/LegalInformation.vue";
import BankingAccount from "@/domains/user/views/account-setting/BankingAccount.vue";
import ManageUser from "@/domains/user/views/manage-user/Index.vue";
import EventFormat from "@/domains/event-format/views/Index.vue";
import EventCategory from "@/domains/event-category/views/Index.vue";
import EventShow from "@/domains/event/views/Show.vue";
import EventTabs from "@/domains/event/views/EventTabs.vue";
import ManageTickets from "@/domains/ticket/views/Index.vue";
import ManagePromo from "@/domains/promo/views/Index.vue";
import { createRouter, createWebHistory } from "vue-router";
import { default as DashboardLayout } from "@/support/layouts/DashboardApp.vue";
import { default as HomepageLayout } from "@/support/layouts/HomepageApp.vue";
import { default as AuthpageLayout } from "@/support/layouts/AuthApp.vue";
import { useAuthStore } from "@/domains/user/stores/authStore";

function requireLogin(to, from, next) {
    const auth = useAuthStore();
    let isLogin = false;
    isLogin = auth.isAuthenticated;

    if (isLogin) {
        next();
    } else {
        next("/login");
    }
}

function guest(to, from, next) {
    const auth = useAuthStore();
    let isLogin;
    isLogin = auth.isAuthenticated;

    if (isLogin) {
        next("/");
    } else {
        next();
    }
}

const routes = [
    {
        path: "/dashboard",
        component: DashboardLayout,
        beforeEnter: requireLogin,
        children: [
            {
                name: "dashboard",
                path: "",
                component: () =>
                    import("@/domains/dashboard/views/Dashboard.vue"),
            },
            {
                path: "/account-settings",
                name: "account-settings",
                component: ProfileTabs,
                children: [
                    {
                        path: "",
                        component: BasicInformation,
                    },
                    {
                        path: "/legal-information",
                        component: LegalInformation,
                    },
                    {
                        path: "/banking-account",
                        component: BankingAccount,
                    },
                ],
            },
            {
                path: "/manage-users",
                name: "manage-users",
                component: ManageUser,
            },
            {
                path: "/event-formats",
                name: "event-formats",
                component: EventFormat,
            },
            {
                path: "/event-categories",
                name: "event-categories",
                component: EventCategory,
            },
            {
                path: "/manage-events",
                children: [
                    {
                        path: "",
                        name: "manage-events",
                        component: () =>
                            import("@/domains/event/views/Index.vue"),
                    },
                    {
                        path: ":id",
                        component: EventTabs,
                        props: (route) => ({
                            id: parseInt(route.params.id),
                        }),
                        children: [
                            {
                                path: "",
                                name: "manage-events.show",
                                component: EventShow,
                                props: (route) => ({
                                    id: parseInt(route.params.id),
                                }),
                            },
                            {
                                path: "tickets",
                                name: "manage-events.tickets",
                                component: ManageTickets,
                                props: (route) => ({
                                    id: parseInt(route.params.id),
                                }),
                            },
                            {
                                path: "promo",
                                name: "manage-events.promo",
                                component: ManagePromo,
                                props: (route) => ({
                                    id: parseInt(route.params.id),
                                }),
                            },
                        ],
                    },
                ],
            },
        ],
    },
    {
        path: "/",
        component: HomepageLayout,
        children: [
            {
                name: "homepage",
                path: "/",
                component: () =>
                    import("@/domains/homepage/frontpage/frontpage.vue"),
            },
            {
                name: "events",
                path: "discover-events",
                component: () =>
                    import(
                        "@/domains/homepage/discover-event/discover-event.vue"
                    ),
            },
        ],
    },
    {
        name: "login",
        path: "/",
        component: AuthpageLayout,
        beforeEnter: guest,
        children: [
            {
                name: "login",
                path: "login",
                component: () => import("@/domains/user/views/auth/login.vue"),
            },
            {
                name: "register",
                path: "register",
                component: "",
            },
        ],
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
