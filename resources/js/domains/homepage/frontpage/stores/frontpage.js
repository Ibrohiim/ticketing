import { Frontpage } from "@/domains/homepage/frontpage/repositories/Frontpage";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useFrontpageStore = defineStore("frontpage", {
    state: () => {
        return {
            links: [],
            meta: {},
            events: [],
            selectedEvent: {},
        };
    },

    actions: {
        async get(options = {}) {
            try {
                this.events = await new Frontpage().get(options);
            } catch (error) {
                toastr.error(error);
            }
        },

        async show(id) {
            try {
                this.selectedEvent = await new Frontpage().show(id);
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
