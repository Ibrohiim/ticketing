import { Repository } from "@/support/Repository";

export class Frontpage extends Repository {
    resource = "/api/frontpage";
}
