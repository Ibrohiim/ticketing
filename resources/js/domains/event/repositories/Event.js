import { Repository } from "@/support/Repository";

export class Event extends Repository {
    resource = "/api/events";

    async getState() {
        try {
            const response = await this.axios.get(`${this.resource}/state`);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async updateBanner(eventId, banner) {
        try {
            const response = await this.axios.post(
                `${this.resource}/${eventId}/update-banner`,
                banner
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }
}
