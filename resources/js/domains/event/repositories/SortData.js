import { Repository } from "@/support/Repository";

export class SortData extends Repository {
    resource = "/api/events/sort-data/types";

    async getTypes() {
        try {
            const response = await this.axios.get(`${this.resource}`);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }
}
