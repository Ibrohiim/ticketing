import { Repository } from "@/support/Repository";

export class EventState extends Repository {
    resource = "/api/events/states";

    async getTypes() {
        try {
            const response = await this.axios.get(`${this.resource}/types`);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }
}
