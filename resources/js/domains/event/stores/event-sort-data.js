import { SortData } from "@/domains/event/repositories/SortData";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useEventSortDataStore = defineStore("eventSortData", {
    state: () => {
        return {
            types: [],
        };
    },

    actions: {
        async getTypes() {
            try {
                this.types = await new SortData().getTypes();
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
