import { Event } from "@/domains/event/repositories/Event";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useEventStore = defineStore("event", {
    state: () => {
        return {
            creatingNew: false,
            editing: false,
            filters: {
                event_category: null,
                event_format: null,
                user: null,
                q: "",
                dates: "",
                sort_data: null,
                state: null,
            },
            links: [],
            meta: {},
            type: ["public", "private"],
            events: [],
            selectedEvent: {},
            showFilterModal: false,
            showModal: false,
            state: [],
        };
    },

    actions: {
        async paginate(options = {}) {
            try {
                const response = await new Event().paginate(options);
                this.events = response.data;
                this.meta = response.meta;
                this.links = response.links;
            } catch (error) {
                toastr.error(error);
            }
        },

        async filter() {
            await this.paginate(this.filters);
        },

        async get(options = {}) {
            try {
                this.events = await new Event().get(options);
            } catch (error) {
                toastr.error(error);
            }
        },

        async show(id) {
            try {
                this.selectedEvent = await new Event().show(id);
            } catch (error) {
                toastr.error(error);
            }
        },

        async store(request) {
            try {
                const response = await new Event().store(request);
                await this.paginate(this.filters);
                this.creatingNew = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async update() {
            try {
                const response = await new Event().update(
                    this.selectedEvent.id,
                    this.selectedEvent
                );
                await this.show(this.selectedEvent.id);
                this.editing = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async destroy() {
            try {
                const response = await new Event().destroy(
                    this.selectedEvent.id
                );
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async updateBanner(eventId, banner) {
            try {
                const response = await axios.post(
                    `/api/events/${eventId}/update-banner`,
                    banner
                );
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async getState() {
            try {
                this.state = await new Event().getState();
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
