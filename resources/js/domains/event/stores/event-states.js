import { EventState } from "@/domains/event/repositories/EventState";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useEventStatesStore = defineStore("eventStates", {
    state: () => {
        return {
            types: [],
        };
    },

    actions: {
        async getTypes() {
            try {
                this.types = await new EventState().getTypes();
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
