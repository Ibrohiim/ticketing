import { Repository } from "@/support/Repository";

export class Promo extends Repository {
    resource = "/api/promo";

    async getByEventId(eventId) {
        try {
            const url = `/api/events/${eventId}/promo-by-event`;
            const response = await this.axios.get(url);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async getByTicketId(eventId, ticketId) {
        try {
            const url = `/api/events/${eventId}/tickets/${ticketId}/promo`;
            const response = await this.axios.get(url);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async destroy(eventId, promoId) {
        try {
            const response = await this.axios.delete(
                `/api/events/${eventId}/promo/${promoId}`
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async getState(eventId) {
        try {
            const response = await this.axios.get(
                `/api/events/${eventId}/promo-state`
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async getType(eventId) {
        try {
            const response = await this.axios.get(
                `/api/events/${eventId}/promo-type`
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }
}
