import { Promo } from "@/domains/promo/repositories/Promo";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const usePromoStore = defineStore("promo", {
    state: () => {
        return {
            promo: [],
            creating: false,
            editing: false,
            links: [],
            meta: {},
            selectedPromo: {},
            showModal: false,
            eventId: "",
            ticketId: "",
            state: [],
            type: [],
        };
    },

    actions: {
        async get() {
            try {
                this.promo = await new Promo().get();
            } catch (error) {
                toastr.error(error);
            }
        },
        async getByTicketId() {
            try {
                this.promo = await new Promo().get(this.eventId, this.ticketId);
            } catch (error) {
                toastr.error(error);
            }
        },

        async getByEventId() {
            try {
                this.promo = await new Promo().getByEventId(this.eventId);
            } catch (error) {
                toastr.error(error);
            }
        },

        async store(request) {
            try {
                const response = await new Promo().store(request);
                await this.getByEventId(this.eventId);
                this.creating = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async update() {
            try {
                const response = await new Promo().update(
                    this.selectedPromo.id,
                    this.selectedPromo
                );
                await this.getByEventId(this.eventId);
                this.editing = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async destroy() {
            try {
                const response = await new Promo().destroy(
                    this.eventId,
                    this.selectedPromo.id
                );
                await this.getByEventId(this.eventId);
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async getState() {
            try {
                this.state = await new Promo().getState(this.eventId);
            } catch (error) {
                toastr.error(error);
            }
        },

        async getType() {
            try {
                this.type = await new Promo().getType(this.eventId);
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
