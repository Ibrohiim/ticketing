import { EventFormat } from "@/domains/event-format/repositories/EventFormat";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useEventFormatStore = defineStore("eventFormat", {
    state: () => {
        return {
            eventFormats: [],
            creating: false,
            editing: false,
            filters: {
                q: "",
            },
            links: [],
            meta: {},
            selectedEventFormat: {},
            showModal: false,
        };
    },

    actions: {
        async paginate(options = {}) {
            try {
                const response = await new EventFormat().paginate(options);
                this.eventFormats = response.data;
                this.meta = response.meta;
                this.links = response.links;
            } catch (error) {
                toastr.error(error);
            }
        },

        async get() {
            try {
                this.eventFormats = await new EventFormat().get();
            } catch (error) {
                toastr.error(error);
            }
        },

        async store(request) {
            try {
                const response = await new EventFormat().store(request);
                await this.paginate(this.filters);
                this.creating = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async update() {
            try {
                const response = await new EventFormat().update(
                    this.selectedEventFormat.id,
                    this.selectedEventFormat
                );
                await this.paginate(this.filters);
                this.editing = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async destroy() {
            try {
                const response = await new EventFormat().destroy(
                    this.selectedEventFormat.id
                );
                await this.paginate(this.filters);
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
