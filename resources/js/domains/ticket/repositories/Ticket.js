import { Repository } from "@/support/Repository";

export class Ticket extends Repository {
    resource = "/api/tickets";

    async get(eventId) {
        try {
            const url = `/api/events/${eventId}/tickets`;
            const response = await this.axios.get(url);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async store(eventId, request) {
        try {
            const response = await this.axios.post(
                `/api/events/${eventId}/tickets`,
                request
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async getState() {
        try {
            const response = await this.axios.get(`${this.resource}/state`);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async getCategory(eventId) {
        try {
            const response = await this.axios.get(
                `/api/events/${eventId}/ticket/category-ticket`
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }
}
