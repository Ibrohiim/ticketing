import { Ticket } from "@/domains/ticket/repositories/Ticket";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useTicketStore = defineStore("ticket", {
    state: () => {
        return {
            tickets: [],
            creating: false,
            editing: false,
            links: [],
            meta: {},
            selectedTicket: {},
            showModal: false,
            eventId: "",
            state: [],
            categories: [],
        };
    },

    actions: {
        async get() {
            try {
                this.tickets = await new Ticket().get(this.eventId);
            } catch (error) {
                toastr.error(error);
            }
        },

        async store(request) {
            try {
                const response = await new Ticket().store(
                    this.eventId,
                    request
                );
                await this.get();
                this.creating = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async update() {
            try {
                const response = await new Ticket().update(
                    this.selectedTicket.id,
                    this.selectedTicket
                );
                await this.get();
                this.editing = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async destroy() {
            try {
                const response = await new Ticket().destroy(
                    this.selectedTicket.id
                );
                await this.get();
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async getState() {
            try {
                this.state = await new Ticket().getState();
            } catch (error) {
                toastr.error(error);
            }
        },

        async getCategory() {
            try {
                this.categories = await new Ticket().getCategory(this.eventId);
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
