import { User } from "@/domains/user/repositories/User";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useUserStore = defineStore("userStore", {
    state: () => {
        return {
            creating: false,
            editing: false,
            filters: {
                q: "",
                role: null,
            },
            links: [],
            meta: {},
            roles: [],
            selectedUser: {},
            showModal: false,
            users: [],
        };
    },

    actions: {
        async paginate(options = {}) {
            try {
                const response = await new User().paginate(options);
                this.users = response.data;
                this.meta = response.meta;
                this.links = response.links;
            } catch (error) {
                toastr.error(error);
            }
        },

        async get(options = {}) {
            try {
                this.users = await new User().get(options);
            } catch (error) {
                toastr.error(error);
            }
        },

        async store(request) {
            try {
                const response = await new User().store(request);
                await this.paginate(this.filters);
                this.creating = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async update() {
            try {
                const response = await new User().update(
                    this.selectedUser.id,
                    this.selectedUser
                );
                await this.paginate(this.filters);
                this.editing = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async destroy() {
            try {
                const response = await new User().destroy(this.selectedUser.id);
                await this.paginate(this.filters);
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async getRoles() {
            try {
                this.roles = await new User().getRoles();
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
