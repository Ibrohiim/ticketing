import { User } from "@/domains/user/repositories/User";
import axios from "axios";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();

export const useAuthStore = defineStore("authStore", {
    state: () => {
        return {
            user: {},
            permissions: [],
            basicInformation: {},
            legalInformation: {},
            banking: {},
        };
    },

    actions: {
        async getAuthenticatedUser() {
            try {
                const response = await axios.get("/api/profile");
                this.user = {
                    id: response.data.id,
                    firstname: response.data.firstname,
                    lastname: response.data.lastname,
                    email: response.data.email,
                    role_name: response.data.role_name,
                    role: response.data.role,
                };

                this.permissions = response.data.permissions;
                this.basicInformation = response.data.basic_information;
                this.legalInformation = response.data.legal_information;
                this.banking = response.data.banking;
            } catch (error) {
                console.log(error);
                toastr.error(error);
            }
        },

        async updateProfile(request) {
            try {
                const response = await new User().updateProfile(request);
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async updateLegalInformation(request) {
            try {
                const response = await new User().updateLegalInformation(
                    request
                );
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async updateBanking(request) {
            try {
                const response = await new User().updateBanking(request);
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async updateProfileImage(request) {
            try {
                const response = await axios.post(
                    "/api/profile/update-image",
                    request
                );
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async updatePassword(request) {
            try {
                const response = await new User().updatePassword(request);
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },
    },

    getters: {
        isAuthenticated: (state) => {
            return Boolean(Object.keys(state.user).length > 0);
        },

        can: (state) => {
            return (permission) => state.permissions.includes(permission);
        },
    },
});
