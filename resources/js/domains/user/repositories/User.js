import { Repository } from "@/support/Repository";

export class User extends Repository {
    resource = "/api/users";

    async getRoles() {
        try {
            const response = await this.axios.get("/api/user-roles");
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async updateProfile(request) {
        try {
            const response = await this.axios.put("/api/profile", request);
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async updateLegalInformation(request) {
        try {
            const response = await this.axios.put(
                "/api/profile/legal-information",
                request
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async updateBanking(request) {
        try {
            const response = await this.axios.put(
                "/api/profile/update-banking",
                request
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }

    async updatePassword(request) {
        try {
            const response = await this.axios.put(
                "/api/profile/update-password",
                request
            );
            return response.data;
        } catch (error) {
            throw error.response.data.message;
        }
    }
}
