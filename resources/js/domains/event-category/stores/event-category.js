import { EventCategory } from "@/domains/event-category/repositories/EventCategory";
import { defineStore } from "pinia";
import { useToastr } from "@/support/toastr";

const toastr = useToastr();
export const useEventCategoryStore = defineStore("eventCategory", {
    state: () => {
        return {
            eventCategories: [],
            creating: false,
            editing: false,
            filters: {
                q: "",
            },
            links: [],
            meta: {},
            selectedEventCategory: {},
            showModal: false,
        };
    },

    actions: {
        async paginate(options = {}) {
            try {
                const response = await new EventCategory().paginate(options);
                this.eventCategories = response.data;
                this.meta = response.meta;
                this.links = response.links;
            } catch (error) {
                toastr.error(error);
            }
        },

        async get() {
            try {
                this.eventCategories = await new EventCategory().get();
            } catch (error) {
                toastr.error(error);
            }
        },

        async store(request) {
            try {
                const response = await new EventCategory().store(request);
                await this.paginate(this.filters);
                this.creating = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async update() {
            try {
                const response = await new EventCategory().update(
                    this.selectedEventCategory.id,
                    this.selectedEventCategory
                );
                await this.paginate(this.filters);
                this.editing = false;
                this.showModal = false;
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },

        async destroy() {
            try {
                const response = await new EventCategory().destroy(
                    this.selectedEventCategory.id
                );
                await this.paginate(this.filters);
                toastr.success(response.message);
            } catch (error) {
                toastr.error(error);
            }
        },
    },
});
