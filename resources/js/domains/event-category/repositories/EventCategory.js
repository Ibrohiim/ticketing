import { Repository } from "@/support/Repository";

export class EventCategory extends Repository {
    resource = "/api/event-categories";
}
