<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title', __('login')) &middot; {{ config('app.name') }}</title>
</head>

<body>
  <div class="container">
    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
      <div class="card mt-5">
        <div class="card-body">
          <form action="{{ route('login') }}" method="post">
            @csrf
            <input name="remember" type="hidden" value="1">

            <div class="mb-3">
              <label class="form-label" for="email">{{ __('email') }}</label>
              <input class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                type="text">

              @error('email')
                <div class="invalid-feedback" role="alert">{{ $message }}</div>
              @enderror
            </div>

            <div class="mb-3">
              <label class="form-label" for="password">{{ __('password') }}</label>
              <input class="form-control" id="password" name="password" type="password">
            </div>

            <button class="btn btn-primary" type="submit">{{ __('login') }}</button>
          </form>
        </div>
      </div>
    </div>

  </div>
  @vite('resources/ts/main.ts')

</body>

</html>
